FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/pkg /app/code
WORKDIR /app/code

# https://github.com/n8n-io/n8n/blob/master/docker/images/n8n/Dockerfile
ARG NODE_VERSION=20.18.0
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

RUN apt-get update && \
    apt-get -y install graphicsmagick recutils asciidoctor pandoc musl && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=npm depName=n8n versioning=semver
ARG N8N_VERSION=1.81.4

# n8n. handlebars and jsonata are just helpful modules that user can enable
RUN chown -R cloudron:cloudron /app/code && \
    gosu cloudron:cloudron npm install n8n@${N8N_VERSION}

# npm config set cache --global /run/npmcache
RUN rm -rf /app/code/node_modules/n8n/dist/public && ln -s /run/n8n/public /app/code/node_modules/n8n/dist/public

RUN rm -rf /home/cloudron/.ssh && ln -s /run/cloudron.ssh /home/cloudron/.ssh

# this allows to use the CLI easily without having to set these
ENV NODE_ENV=production
ENV N8N_RELEASE_TYPE=stable
ENV N8N_USER_FOLDER="/app/data/user"
ENV N8N_CUSTOM_EXTENSIONS="/app/data/custom-extensions"

# put n8n binary in the path
ENV PATH=/app/code/node_modules/.bin:$PATH

COPY start.sh env.sh.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
