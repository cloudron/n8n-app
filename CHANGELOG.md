[0.1.0]
* Initial version

[0.2.0]
* Update description

[0.3.0]
* Use latest version 
* Fix Firefox
* Allow custom configuration

[1.0.0]
* Update to latest version 0.127.0
* add stable testing for future updates
* change manifest information

[1.0.1]
* Update n8n to 0.130.0

[1.1.0]
* Update n8n to 0.131.0

[1.1.1]
* Fix manual executing loading forever

[1.2.0]
* Update n8n to 0.132.1
* remove supervisord for nginx and n8n
* run n8n directly

[1.3.0]
* Update n8n to 0.132.2
* disable update notifications
* run as normal cloudron user

[1.4.0]
* Update n8n to 0.133.0
* Remove sendmail addon, please create a mailbox in Cloudro or outside Cloudron and use those credentials instead.

[1.4.1]
* Update n8n to 0.134.0

[1.5.0]
* Update n8n to 0.135.1

[1.5.1]
* Update n8n to 0.135.2

[1.6.0]
* Update n8n to 0.136.0

[1.7.0]
* Update n8n to 0.137.0

[1.8.0]
* Update n8n to 0.138.0

[1.9.0]
* Update n8n to 0.139.0

[1.10.0]
* Update n8n to 0.140.0

[1.11.0]
* Update n8n to 0.141.0

[1.11.1]
* Update n8n to 0.141.1

[1.12.0]
* Update n8n to 0.142.0

[1.13.0]
* Update n8n to 0.143.0

[1.14.0]
* Update n8n to 0.144.0

[1.15.0]
* Update n8n to 0.145.0

[1.16.0]
* Update n8n to 0.146.0
* Re-work data directory layout to be more clear
* Support for using [custom node modules](https://docs.cloudron.io/apps/n8n/#custom-node-modules)

[1.17.0]
* Update n8n to 0.147.1

[1.18.0]
* Update n8n to 0.148.0

[1.19.0]
* Update n8n to 0.149.0

[1.20.0]
* Update n8n to 0.150.0

[1.21.0]
* Update n8n to 0.151.0

[1.22.0]
* Update n8n to 0.152.0

[1.23.0]
* Update n8n to 0.153.0

[1.24.0]
* Update n8n to 0.155.1

[1.24.1]
* Update n8n to 0.155.2

[1.25.0]
* Update n8n to 0.156.0

[1.25.1]
* Update n8n to 0.157.0

[1.25.2]
* Update n8n to 0.157.0

[1.25.3]
* Update n8n to 0.157.1

[1.26.0]
* Update n8n to 0.158.0

[1.26.1]
* Add `marked` npm module

[1.26.2]
* Update n8n to 0.159.0

[1.26.3]
* Update n8n to 0.159.1

[1.27.0]
* Update n8n to 0.161.0

[1.27.1]
* Update n8n to 0.161.1

[1.28.0]
* Update n8n to 0.162.0

[1.28.1]
* Update n8n to 0.163.1

[1.29.0]
* Update n8n to 0.164.1

[1.30.0]
* Update n8n to 0.165.1

[1.31.0]
* Update n8n to 0.166.0

[1.32.0]
* Add pandoc and asciidoctor

[1.32.1]
* Update n8n to 0.167.0

[2.0.0]
* Update n8n to 0.168.2
* Cloudron proxyAuth is removed, since n8n now has its own usermanagement. Make sure to create local users within n8n from now on to keep workflows private.

[2.0.1]
* Integrate with Cloudron's email sending addon

[2.1.0]
* Update n8n to 0.169.0

[2.2.0]
* Update n8n to 0.170.0

[2.2.1]
* Update n8n to 0.171.0

[2.2.2]
* Update n8n to 0.171.1

[2.2.3]
* Update n8n to 0.172.0

[2.2.4]
* Update n8n to 0.173.0

[2.2.5]
* Update n8n to 0.173.1

[2.3.0]
* Update n8n to 0.174.0

[2.3.1]
* Update n8n to 0.175.0

[2.3.2]
* Update n8n to 0.175.1

[2.3.3]
* Update n8n to 0.176.0

[2.4.0]
* Update n8n to 0.177.0

[2.4.1]
* Update n8n to 0.178.0

[2.4.2]
* Update n8n to 0.178.1

[2.4.3]
* Update n8n to 0.178.2

[2.4.4]
* Update n8n to 0.179.0

[2.4.5]
* Update n8n to 0.180.0

[2.5.0]
* Update n8n to 0.181.0
* Enable easier CLI use

[2.5.1]
* Update n8n to 0.181.1

[2.6.0]
* Update n8n to 0.181.2

[2.7.0]
* Update n8n to 0.182.1

[2.8.0]
* Update n8n to 0.183.0

[2.8.1]
* Update n8n to 0.184.0

[2.8.2]
* Update n8n to 0.185.0

[2.8.3]
* Update n8n to 0.186.0

[2.8.4]
* Update n8n to 0.186.1

[2.8.5]
* Update n8n to 0.167.1

[2.8.6]
* Update n8n to 0.167.2

[2.8.7]
* Update n8n to 0.188.0

[2.8.8]
* Update n8n to 0.189.0

[2.8.9]
* Update n8n to 0.189.1

[2.8.10]
* Update n8n to 0.190.0

[2.8.11]
* Update n8n to 0.191.0

[2.8.12]
* Update n8n to 0.191.1

[2.8.13]
* Update n8n to 0.192.0

[2.8.14]
* Update n8n to 0.192.1

[2.8.15]
* Update n8n to 0.192.2

[2.8.16]
* Update n8n to 0.193.1

[2.8.17]
* Update n8n to 0.193.2

[2.8.18]
* Update n8n to 0.193.3

[2.8.19]
* Update n8n to 0.193.4

[2.8.20]
* Update n8n to 0.193.5

[2.8.21]
* Update n8n to 0.194.0

[2.8.22]
* Update n8n to 0.195.3

[2.8.23]
* Update n8n to 0.195.4

[2.8.24]
* Update n8n to 0.195.5

[2.9.0]
* Update n8n to 0.197.1

[2.10.0]
* Update n8n to 0.198.0

[2.11.0]
* Update n8n to 0.198.2

[2.12.0]
* Update n8n to 0.199.0

[2.13.0]
* Update n8n to 0.200.0

[2.13.1]
* Update n8n to 0.200.1

[2.14.0]
* Update n8n to 0.201.0

[2.14.1]
* Fix formatting of display name

[2.15.0]
* Update n8n to 0.202.1

[2.16.0]
* Update n8n to 0.203.0

[2.16.1]
* Update n8n to 0.203.1

[2.16.2]
* Fix installation of community modules
* Add ajv-formats module

[2.17.0]
* Update n8n to 0.204.0

[2.18.0]
* Update n8n to 0.205.0

[2.19.0]
* Update n8n to 0.206.1

[2.20.0]
* Update n8n to 0.207.0

[2.20.1]
* Update n8n to 0.207.1

[2.21.0]
* Update n8n to 0.208.0

[2.21.1]
* Update n8n to 0.208.1

[2.22.0]
* Update n8n to 0.209.1

[2.22.1]
* Update n8n to 0.209.2

[2.22.2]
* Update n8n to 0.209.3

[2.22.3]
* Update n8n to 0.209.4

[2.23.0]
* Update n8n to 0.210.0

[2.23.1]
* Update n8n to 0.210.1

[2.24.0]
* Update n8n to 0.211.1

[2.24.1]
* Update n8n to 0.211.2

[2.25.0]
* Update n8n to 0.212.0

[2.25.1]
* Update n8n to 0.212.1

[2.26.0]
* Update n8n to 0.213.0

[2.27.0]
* Update n8n to 0.214.0

[2.27.1]
* Update n8n to 0.214.1

[2.27.2]
* Update n8n to 0.214.2

[2.27.3]
* Update n8n to 0.214.3

[2.28.0]
* Update n8n to 0.215.0

[2.28.1]
* Update n8n t 0.215.1

[2.28.2]
* Update n8n to 0.215.2

[2.29.0]
* Update n8n to 0.216.0

[2.29.1]
* Update n8n to 0.216.1

[2.30.0]
* Update n8n to 0.217.0

[2.30.2]
* Update n8n to 0.217.1

[2.31.0]
* Rename env to env.sh

[2.32.0]
* Update n8n to 0.218.0

[2.32.1]
* Update n8n to 0.219.0

[2.32.2]
* Update n8n to 0.219.1

[2.33.0]
* Update n8n to 0.220.0

[2.33.1]
* Update n8n to 0.220.1

[2.34.0]
* Update n8n to 0.221.0

[2.34.1]
* Update n8n to 0.221.1

[2.34.2]
* Update n8n to 0.221.2

[2.35.0]
* Add odoo-xmlrpc

[2.36.0]
* Update n8n to 0.222.1
* AWS SNS Node: Fix an issue with messages failing to send if they contain certain characters (#5807) (f0954b9)
* core: augmentObject should clone Buffer/Uint8Array instead of wrapping them in a proxy (#5902) (a877b02)
* core: augmentObject should use existing property descriptors whenever possible (#5872) (b1ee8f4)
* core: Fix the issue of nodes not loading when run via npx (#5888) (163446c)
* core: Improve axios error handling in nodes (#5891) (f0a51a0)
* core: Password reset should pass in the correct values to external hooks (#5842) (3bf267c)
* core: Prevent augmentObject from creating infinitely deep proxies (#5893) (6906b00), closes #5848
* core: Use table-prefixes in queries in import commands (#5887) (de58fb9)
* editor: Fix focused state in Code node editor (#5869) (3be37e2)
* editor: Fix loading executions in long execution list (#5843) (d5d9f58)
* editor: Show correct status on canceled executions (#5813) (00181cd)
* Gmail Node: Gmail luxon object support, fix for timestamp (695fabb)
* HTTP Request Node: Detect mime-type from streaming responses (#5896) (0be1292)
* HubSpot Trigger Node: Developer API key is required for webhooks (918c79c)
* Set Node: Convert string to number (72eea0d)

[2.37.0]
* Update n8n to 0.223.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.223.0)
* Add droppable state for booleans when mapping (#5838) (e3884ce)
* AWS SNS Node: Fix an issue with messages failing to send if they contain certain characters (#5807) (32c4eef)
* Compare Datasets Node: Fuzzy compare not comparing keys missing in one of inputs (d1945d9)
* Compare Datasets Node: Support for dot notation in skip fields (83e25c0)
* core: augmentObject should clone Buffer/Uint8Array instead of wrapping them in a proxy (#5902) (a721734)
* core: augmentObject should use existing property descriptors whenever possible (#5872) (6a1b7c3)
* core: Deactivate active workflows during import (#5840) (fa5bc81)
* core: Do not mark duplicates as circular references in jsonStringify (#5789) (18efaf3)
* core: Do not user util.types.isProxy for tracking of augmented objects (#5836) (aacbb54)
* core: Fix curl import error when no data (085660d)
* core: Fix the issue of nodes not loading when run via npx (#5888) (e47190b)
* core: Handle Date and RegExp correctly in jsonStringify (#5812) (4f91525)
* core: Handle Date and RegExp objects in AugmentObject (#5809) (6c35ffa)
* core: Improve axios error handling in nodes (#5891) (a260c05)
* core: Password reset should pass in the correct values to external hooks (#5842) (5bcab8f)
* core: Prevent augmentObject from creating infinitely deep proxies (#5893) (31cd04c), closes #5848
* core: Service account private key as a password field (739b9b0)
* core: Update lock file (#5801) (06d7a46)
* core: Prevent non owners password reset when saml is enabled (#5788) (2216455)
* core: Use table-prefixes in queries in import commands (#5887) (ddbfcc7)
* core: Waiting workflows not stopping (#5811) (744c3fd)
* Date & Time Node: Add info box at top of date and time explaining expressions (b7a20dd)
* Date & Time Node: Convert luxon DateTime object to ISO (7710652)
* editor: Add $if, $min, $max to root expression autocomplete (#5858) (a13866e)
* editor: Curb overeager item access linting (#5865) (3ae6933)
* editor: Disable Grammarly in expression editors (#5826) (ddc8f30)
* editor: Disable password reset on desktop with no user management (#5853) (96533a9)
* editor: Fix connection lost hover text not showing (#5828) (b69129b)
* editor: Fix focused state in Code node editor (#5869) (48446f5)
* editor: Fix issue preventing execution preview loading when in an iframe (#5817) (d86e693)
* editor: Fix loading executions in long execution list (#5843) (5c9343c)
* editor: Fix mapping with special characters (#5837) (f8f584c)
* editor: Prevent error from showing-up when duplicating unsaved workflow (#5833) (0b0024d)
* editor: Prevent NDV schema view pagination (#5844) (1eba478)
* editor: Show correct status on canceled executions (#5813) (d0788ee)
* editor: Support backspacing with modifier key (#5845) (11692c5)
* Gmail Node: Gmail luxon object support, fix for timestamp (2b9ca0d)
* Google Sheets Node: Fix insertOrUpdate cell update with object (0625e2e)
* HTML Extract Node: Support for dot notation in JSON property (0da3b96)
* HTTP Request Node: Detect mime-type from streaming responses (#5896) (69efde7)
* HTTP Request Node: Fix AWS credentials to stop removing url params for STS (#5790) (a1306c6)
* HTTP Request Node: Refresh token properly on never fail option (#5861) (33c67f4)
* HTTP Request Node: Support for dot notation in JSON body (b29cf9a)
* HubSpot Trigger Node: Developer API key is required for webhooks (e11a30a)
* LinkedIn Node: Update the version of the API (#5720) (18d2e7c)
* Redis Node: Fix issue with hash set not working as expected (#5832) (db25441)
* Set Node: Convert string to number (b408550)
* core: Read ephemeral license from environment and clean up ee flags (#5808) (83aef17)
* editor: Allow tab to accept completion (#5855) (1b8c35a)
* editor: Enable saving workflow when node details view is open (#5856) (0a59002)
* editor: SSO onboarding (#5756) (04f8600)
* editor: SSO setup (#5736) (f4e5949), closes #5899
* Filter Node: Show discarded items (f7f9d91)
* HTTP Request Node: Follow redirects by default (#5895) (f7e610b)
* Postgres Node: Overhaul node (07dc0e4)
* ServiceNow Node: Add support for work notes when updating an incident (#5791) (1409f5d)
* SSH Node: Hide the private key within the ssh credential (#5871) (d877361)

[2.38.0]
* Update n8n to 0.224.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.224.0)
* Code Node: Update vm2 to address CVE-2023-29017 (#5947) (f0eba0a)
* core: App should not crash with a custom rest endpoint (#5911) (2881ee9), closes #5880
* core: Do not execute workflowExecuteBefore hook when resuming executions from a waiting state (#5727) (6689451)
* core: Fix issue where sub workflows would display as running forever after failure to start (#5905) (3e382ef)
* core: Update xml2js to address CVE-2023-0842 (#5948) (3085ed9)
* editor: Drop mergeDeep in favor of lodash merge (#5943) (0570514)
* HTTP Request Node: Show detailed error message in the UI again (#5959) (e79679c)
* Create TOTP node (#5901) (6cf74e4)
* editor: Add user activation survey (#5677) (725393d)
* editor: SAML login disables Invite button (#5922) (3fdc441)
* editor: SAML paywall state (#5906) (d40e86a)

[2.38.1]
* Update n8n to 0.224.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.224.1)
* core: Fix broken API permissions in public API (#5978) (b76ab31)
* editor: Only treat as CTRL pressed by default on touch devices for MouseEvent (#5968) (471be3b)

[2.39.0]
* Update n8n to 0.225.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.225.0)
* core: Fix broken API permissions in public API (#5978) (49d838f)
* core: Fix paired item returning wrong data (#5898) (b13b7d7)
* core: Improve SAML connection test result views (#5981) (4c994fa)
* core: Make getExecutionId available on all nodes types (#5990) (c42820e)
* core: Skip SAML onboarding for users with first- and lastname (#5966) (8474cd3)
* editor: Add padding to prepend input (#5874) (cd89489)
* editor: Cleanup demo/video experiment (#5974) (c171365)
* editor: Enterprise features missing with UM (#5995) (f9a810a)
* editor: Fix moving canvas on middle click preventing lasso selection (#5996) (3c2a569)
* editor: Make sure to redirect to blank canvas after personalization modal (#5980) (7c474d3)
* editor: Only treat as CTRL pressed by default on touch devices for MouseEvent (#5968) (536d810)
* editor: Fix n8n-checkbox alignment (#6004) (f544826)
* Code Node: Consistently handle various kinds of data returned by user code (#6002) (f9b3aea)
* Github Trigger Node: Remove content_reference event (#5830) (d288a91)
* Google Sheets Trigger Node: Return actual error message (ba5b4eb)
* HTTP Request Node: Fix itemIndex in HTTP Request errors (#5991) (b351c62)
* NocoDB Node: Fix for updating or deleting rows with not default primary keys (ee7f863)
* OpenAI Node: Update models to only show those supported (#5805) (29959be)
* OpenAI Node: Update OpenAI Text Moderate input placeholder text (#5823) (6b9909b)
* core: Add variables feature (#5602) (1bb9871)
* core: Add versionControl feature flag (#6000) (33299ca)
* core: Support for google service account in HTTP node (0b48088)
* editor: Add Ask AI preview (#5916) (f8f8374)
* GitLab Node: Add Additional parameters for File List (#5621) (3810039)
* MySQL Node: Overhaul (0a53c95)

[2.39.1]
* Update n8n to 0.225.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.225.1)
* editor: Clean up demo and template callouts from workflows page (#6023) (6ec1c45)
* editor: Fix memory leak in Node Detail View by correctly unsubscribing from event buses (#6021) (1b9e047)
* editor: SettingsSidebar should disconnect from push when navigating away (#6025) (e9f8cfe)
* Notion Node: Update credential test to not require user permissions (#6022) (6d02ae5)

[2.39.2]
* Update n8n to 0.225.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.225.2)
* core: Upgrade google-timezones-json to use the correct timezone for Sao Paulo (#6042) (f93fd5a), closes #2647
* Code Node: Update vm2 to address CVE-2023-30547 (#6039) (f1ca4e2)

[2.40.0]
* Update n8n to 0.226.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.226.0)
* core: Add license:info command (#6047) (ab12d3e)
* core: Add SSH key generation (#6006) (71ed1f4)
* core: Add support for digestAuth to httpRequest and declarative style (#5676) (62f993c)
* core: Manage version control settings (#6079) (f3b4701)
* core: Upgrade google-timezones-json to use the correct timezone for Sao Paulo (#6042) (b8cb5d7), closes #2647
* editor: Add disable template experiment (#5963) (a74284b)
* editor: Add SQL editor support (#5517) (70aaf24)
* editor: Enhance Node Creator actions view (#5954) (390841b)
* editor: Version control (WIP) (#6013) (0e0a064)
* editor: Version control paywall (WIP) (#6030) (ef79b03)
* Google BigQuery Node: Node improvements (#4877) (9817a15)

[2.40.1]
* Update n8n to 0.226.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.226.1)
* Compression Node: Fix issue with decompression failing with uppercase extensions (#6098) (7136500)
* core: Account for nodes with renamable content (#6109) (b561d46)
* core: Fix hasOwnProperty on augmented objects (#6124) (2f015c0)
* core: Fix canceled execution status (#6142) (1796101)
* core: Skip auth for controllers/routes that don't use the Authorized decorator, or use Authorized('none') (#6106) (9d44991)
* Correctly allow sharees to test credential when opening the modal (#6111) (240bb47)
* Date & Time Node: Numbers conversions fix (e11e7cd)
* editor: Change execution list tab loader design (#6120) (ffc033f)
* editor: Fix Show details summary (#6113) (e12bafb)
* editor: Fix copy selection behavior (#6112) (0efd94a)
* editor: Fix cropped off completions docstrings (#6129) (06594cc)
* editor: Fix missing Stop Listening button (#6125) (dcbd2d2)
* editor: Fix quote handling on dollar-sign variable completions (#6128) (c23ad35)
* editor: Fix sidebar button styling (#6138) (d3f4bc1)
* editor: Fix unique names for node duplication (#6134) (48a4068)
* editor: Fix unscrollable node settings (#6133) (f762f16)
* editor: Loading state for executions tab (#6100) (2e12c50)
* editor: Remove pagination from binary data output (#6093) (7b7d9de)
* editor: Show error in RLC if credentials are not set (#6108) (5bf3400)
* HTTP Request Node: Add description for 'Specify Body' option (#6114) (69b6ba8)
* HTTP Request Node: Always lowercase headers (31c56a1)
* Mattermost Node: Fix base url trailing slash error (#6097) (788fda1)
* Merge Node: Do not error if expected key is missing (8b59564)
* Prevent displaying an endless timer in the execution list for finished executions (#6137) (2672896)
* Slack Node: Restore ability to send text in addition of blocks or attachments (625d672)

[2.40.2]
* Update n8n to 0.226.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.226.2)
* core: Fix bug running addUserActivatedColumn migration on MariaDB (#6157) (aa8e96d)

[2.41.0]
* Update n8n to 0.227.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.227.0)
* core: Account for nodes with renamable content (#6109) (c99f158)
* core: Assign Unknown Error only if message or description not present in error (8aedc03)
* core: Avoid using Object.keys on Buffer and other non-plain objects (#6131) (a3aba83)
* core: Better error message in Webhook node when using the POST method (a0dd17e)
* core: Better errors for common status codes fix (700cc39)

[2.42.0]
* Update n8n to 0.228.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.228.1)

[2.42.1]
* Update n8n to 0.228.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.228.2)
* Code Node: Restore help text (#6231) (6bf5c51)
* core: Make sure that special polling parameters are available on community nodes as well (#6230) (49391a2)

[2.43.0]
* Update n8n to 0.229.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.229.0)
* Code Node: Restore help text (#6231) (e72d564)
* core: Make sure that special polling parameters are available on community nodes as well (#6230) (9db49d0)
* Remove workflow execution credential error message when instance owner (#6116) (e81a964)
* core: Reduce the number of events sent to Sentry (#6235) (a4c0cc9)
* core: Return OAuth2 error body if available (#5794) (79d0a0f)
* editor: Add cloud ExecutionsUsage and API blocking using licenses (#6159) (cd7c312), closes #6187
* editor: Add color picker design system component (#6179) (823e885)
* editor: Drop support for legacy browsers that do not have native ESM support (#6239) (9182d15)
* editor: Updating node reference pattern in expression editor (#6228) (13bcec1)
* editor: Version Control settings update (WIP) (#6233) (0666377)
* Google Ads Node: Update to support v13 (#6212) (bd1bffc)
* Respond to Webhook Node: Move from Binary Buffer to Binary streaming (#5613) (8ae2d80)

[2.44.0]
* Update n8n to 0.230.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.230.0)
* core: Remove all floating promises. Enforce @typescript-eslint/no-floating-promises (#6281) (e046f65)
* core: Replace client-oauth2 with an in-repo package (#6266) (a1b1f24)
* Execution Data Node: New node (#6247) (3f7c4f0)
* Gotify Node: Add support for self signed certificates (#6053) (401cffd)
* Ldap Node: Add LDAP node (#4783) (ec393bc)
* LoneScale Node: Add LoneScale node and Trigger node (#5146) (4b85433)
* RabbitMQ Node: Add mode for acknowledging and deleting from queue later in workflow (#6225) (f5950b2)
* Send Email Node: Add content-id for email attachments (#3632) (8fe8aad)

[2.44.1]
* Update n8n to 0.230.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.230.1)
* editor: Fix locale plularisation if count is 0 (#6312) (33b6b28)
* Execute Command Node: Block executions when command is empty (#6308) (c8b88a1)

[2.44.2]
* Update n8n to 0.230.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.230.2)
* Code Node: Update vm2 to address CVE-2023-32313 (#6318) (4301127)
* core: Optimize getSharedWorkflowIds query (#6314) (4269d3f)
* core: Prevent prototype pollution on injectable services (#6309) (a4bff84)

[2.45.0]
* Update n8n to 0.231.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.231.0)
* Code Node: Fix item and items alias regression (#6331) (54e3838)
* Code Node: Update vm2 to address CVE-2023-32313 (#6318) (bcbec52)
* core: Optimize getSharedWorkflowIds query (#6314) (0631f69)
* core: Prevent prototype pollution on injectable services (#6309) (d94c20a)
* editor: Fix locale plularisation if count is 0 (#6312) (0d88bd7)
* editor: Fix Luxon date parsing of ExecutionsUsage component (#6333) (8f0ff46)
* editor: Update SSO settings styles (#6342) (5ae1124)
* Execute Command Node: Block executions when command is empty (#6308) (011d577)
* Show Ask AI only on Code Node (#6336) (da856d1)
* Add manual login option and password reset link for SSO (#6328) (77e3f15)
* core: Add metadata (customdata) to event log (#6334) (792b1c1)
* editor: Implement Resource Mapper component (#6207) (04cfa54), closes #5752 #5814

[2.45.1]
* Update n8n to 0.231.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.231.1)
* editor: Fix an issue with connections breaking during renaming (#6358) (481ca36)
* editor: Fix typing $ in inline expression field reloading node parameters form (#6374) (ff3c60a)

[2.46.0]
* Update n8n to 0.232.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.232.0)
* Crypto Node: Add support for hash and hmac on binary data (#6359) (406a405)
* editor: Make WF name a link on /executions (#6354) (6ddf161)
* New trigger PostgreSQL (#5495) (4488f93)
* Version control mvp (#6271) (1b32141)

[2.47.0]
* Update n8n to 0.233.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.233.0)
* core: Allow all executions to be stopped (#6386) (cc44af9)
* core: Prevent arbitrary code execution via expressions (#6420) (da7ae2b)
* editor: Hide version control main menu component if no feature flag (#6419) (75c0ab0)

[2.47.1]
* Update n8n to 0.233.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.233.1)
* Fix the url sent in the password-reset emails (#6466) (85300fc)

[2.48.0]
* Update n8n to 0.234.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@0.234.0)
* Add support for large files with declarative nodes (#6461) (e0f109f)
* AwsS3 Node: Small overhaul of the node with multipart uploading (#6017) (109442f)
* core: Add GET /users endpoints to public API (#6360) (6ab3502)
* core: Add PKCE for OAuth2 (#6324) (fc7261a)
* DebugHelper Node: Fix and include in main app (#6406) (18f5884)
* Gmail Node: Add reply to email (#6453) (fddc69e)
* Item Lists Node: Improvements (#6190) (1dbca44)
* Migrate integer primary keys to nanoids (#6345) (c3ba012), closes #6323
* Stripe Trigger Node: Add action required trigger for payment intents (#6490) (f2154fb)
* Webhook Node: Stream binary response in lastNode.firstEntryBinary mode (#6463) (6ccab3e)

[3.0.0]
* Update n8n to 1.0.1
* [Breaking changes for version 1.0](https://docs.n8n.io/1-0-preview/#expected-breaking-changes)
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.0.0)
* core Docker containers now run as the user node instead of root (#6365) (f636616)
* core Drop debian and rhel7 images (#6365) (f636616)
* core Drop support for deprecated WEBHOOK_TUNNEL_URL env variable (#6363)
* core Execution mode defaults to main now, instead of own (#6363)
* core Default push backend is websocket now, instead of sse (#6363)
* core Stop loading custom/community nodes from n8n's node_modules folder (#6396) (a45a2c8)
* core User management is mandatory now. basic-auth, external-jwt-auth, and no-auth options are removed (#6362) (8c008f5)
* core Allow syntax errors and expression errors to fail executions (#6352) (1197811)
* core Drop support for request library and N8N_USE_DEPRECATED_REQUEST_LIB env variable (#6413) (632ea27)
* core Make date extensions outputs match inputs (#6435) (85372aa)
* core Drop support for executeSingle method on nodes (#4853) (9194d8b)
* core Change data processing for multi-input-nodes (#4238) (b8458a5)
* core: All migrations should run in a transaction (#6519) (e152cfe)
* Edit Image Node: Fix transparent operation (#6513) (4a4bcbc)
* Google Drive Node: URL parsing (#6527) (18aa9f3)
* Google Sheets Node: Incorrect read of 0 and false (#6525) (b6202b5)
* Merge Node: Enrich input 2 fix (#6526) (70822ce)
* Notion Node: Version fix (#6531) (d3d8522)
* Show error when referencing node that exist but has not been executed (#6496) (3db2707)
* core: Change node execution order (most top-left one first) (#6246) (0287d5b)
* core: Remove conditional defaults in V1 release (#6363) (f636616)
* editor: Add v1 banner (#6443) (0fe415a)
* editor: SQL editor overhaul (#6282) (beedfb6)
* HTTP Request Node: Notice about dev console (#6516) (d431117)

[3.0.1]
* Ensure we set the app domain for webhooks

[3.0.2]
* Add libmusl to support some custom nodes

[3.0.3]
* Update n8n to 1.0.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.0.2)
* core: Improve the performance of last 2 sqlite migrations (#6522) (718a89d)
* core: Reduce memory consumption on BinaryDataManager.init (#6633) (39665b7)
* core: Remove typeorm patches, but still enforce transactions on every migration (#6594) (8933e9b)
* core: Update docker compose setup for V1 images (#6642) (cfa21bd)
* editor: Show retry information in execution list only when it exists (#6587) (faa2bcc)
* Brevo Node: Rename SendInBlue node to Brevo node (#6521) (81de96b)
* Code Node: Install python modules always in a user-writable folder (#6568) (5abe530)
* Google Drive Node: Fix regex in file RLC (#6607) (beba99f)
* HTTP Request Node: Cleanup circular references in response (#6590) (2151594)
* Postgres Node: Upsert does not fetch columns when schema other then public (#6643) (2e55cd9)
* Salesforce Node: Fix typo for adding a contact to a campaign (#6598) (66f3cb6)

[3.0.4]
* Update n8n to 1.0.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.0.3)
* API: Implement users account quota guards (#6434) (7cd074e), closes #6636
* Telegram Node: Add support for sending messages to forum topics (#5746) (b971fa9)

[3.0.5]
* Update n8n to 1.0.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.0.4)
* API: Do not add starting node on workflow creation (#6686) (597f2c7)
* API: Fix issue with workflow setting not supporting newer nanoids (#6699) (1a3a90f)
* core: Deleting manual executions should defer deleting binary data (#6680) (d8970db)
* core: Filter out workflows that failed to activate on startup (#6676) (bc16afd)
* core: Handle all uncaught exception, not just the ones from Axios (#6666) (975fc05)

[3.0.6]
* Update n8n to 1.0.5
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.0.5)
* core: Add missing indices on sqlite (#6673) (f9c24c9)
* core: Redirect user to previous url after SSO signin (#6710) (982f6f5)
* core: Support redis cluster in queue mode (#6708) (0b9ae37)
* core: Use lower cased email for SAML email attribute (#6663) (7b390ae)
* editor: Add paywall state to non owner users for Variables (#6679) (1aa1484)
* editor: Extend menu item and use it as a recursive component (#6618) (ee602bb)
* editor: Increase contrast ratio in execution list workflow names (#6661) (77f2cfc)
* editor: Show appropriate empty workflow list content when instance environment is readonly (#6610) (0a87198)
* editor: Update design system menu item component (#6659) (902eb85)
* FTP Node: List recursive ignore . and .. to prevent infinite loops (#6707) (da89668)
* GitLab Trigger Node: Fix trigger activation 404 error (#6711) (f463ded)
* Google BigQuery Node: Error description improvement (#6715) (7c8e534)

[3.1.0]
* Update n8n to 1.1.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.1.0)
* Add crowd.dev node and trigger node (#6082) (238a78f)
* Add missing input panels to some trigger nodes (#6518) (fdf8a42)
* Add various source control improvements (#6533) (68fdc20)
* Airtable Node: Overhaul (#6200) (b69d20c)
* Allow eslint-config to be externally consumable (#6694) (3566c13)
* Allow hiding credential params on cloud (#6687) (2af1c24)
* API: Implement users account quota guards (#6434) (e5620ab), closes #6636
* core: Add cache service (#6729) (c0d2bac)
* core: Only show V1 banner to users who migrated (#6622) (071e56f)
* editor: Implement new banners framework (#6603) (4240e76)
* editor: Load fixed template list as experiment (#6632) (e996622)
* editor: Prevent saving of workflow when canvas is loading (#6497) (f89ef83)
* editor: Removing ph-no-capture class from some elements (#6674) (c3455a4)
* Environments release using source control (#6653) (fc7aa8b)
* Google Cloud Storage Node: Use streaming for file uploads (#6462) (cd0e41a)
* Google Drive Node: Overhaul (#5941) (d70a1cb)
* HTML Node: 'Convert to table operation (#6540) (8abb03d)
* HTTP Request Node: New http request generic custom auth credential (#5798) (b17b458)
* Matrix Node: Allow setting filename if the binary data has none (#6536) (8b76e98)
* Microsoft To Do Node: Add an option to set a reminder when creating a task (#5757) (b19833d)
* Notion Node: Add option to update icon when updating a page (#5670) (225e849)
* OpenAI Node: Update max token limit to support newer model limits (#6644) (26046f6)
* Read PDF Node: Replace pdf-parse with pdfjs, and add support for streaming and encrypted PDFs (#6640) (0a31b8e)
* Rundeck Node: Add support for node filters (#5633) (1f70f49)
* Slack Node: Add option to include link to workflow in Slack node (#6611) (aa53c46)
* Strava Node: Add hide_from_home field in Activity Update (#5883) (7495e31)
* Telegram Node: Add support for sending messages to forum topics (#5746) (e6a81f0)
* Twitter Node: Node overhaul (#4788) (42721db)

[3.1.1]
* Update n8n to 1.1.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.1.1)
* core: Allow ignoring SSL issues on generic oauth2 credentials (#6702) (45dcbd4)
* editor: Display source control buttons properly (#6756) (41e3c43)
* Lemlist Node: Fix pagination issues with campaigns and activities (#6734) (3865add)

[3.2.0]
* Update n8n to 1.2.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.2.0)
* Auth.api user limit test expecting incorrect status (#6836) (371bfa0)
* Code Node: Consistent redirection of stdout for JS and Python sandboxes (#6818) (f718c22)
* core: Add missing primary key on the execution_data table on postgres (#6797) (dc295ac)
* core: Add sharing data to workflows in EE executions (#6872) (6796d9e)
* core: Allow ignoring SSL issues on generic oauth2 credentials (#6702) (feac369)
* core: Change VariablesService to DI and use caching (#6827) (659ca26)
* core: Fix loading of scoped-community packages (#6807) (53e58b4)
* core: Fix property existence checks on AugmentObject (#6842) (732416f)
* core: Fix source control name and email being switched (#6839) (6ec7033)
* core: Fix WebSocket close codes (a8bfb46)
* core: Log crash causes to console when sentry is disabled (#6890) (6553d92)
* core: OAuth1 authentication fix for Clever Cloud API (#6847) (5ab30fd)
* core: Restrict read/write file paths access (#6582) (f6bf9e9)
* core: Serialize BigInts (#6805) (7b27fa5)
* core: Update packages to address CVE-2023-2142 and CVE-2020-28469 (#6844) (a5667e6)
* Correct typos in Taiga and ServiceNow nodes (#6814) (803b152)
* Display source control buttons properly (#6756) (d050b99)
* editor: Close tags dropdown when modal is opened (#6766) (cf00ba7), closes #6571
* editor: Do not show mapping discoverability tooltip after dismiss (#6862) (08982ed)
* editor: Fix code node highlight error (#6791) (50b0dc2)
* editor: Fix collapsed sub menu elements (#6778) (d33528d)
* editor: Fix credential errors in executions view for workflow sharee (#6875) (a0f9b2e)
* editor: Fix redo when adding node on connection (#6833) (4ac4b85)
* editor: Fix tooltip opening delay prop name (#6776) (e19b0d7)
* editor: Fix value syncing in SQL and HTML editor (#6848) (90e825f)
* editor: Improve displaying and hiding of connections actions (#6823) (369a2e9)
* editor: Prevent text edit dialog from re-opening in same tick (#6781) (c9f3acc)
* editor: Remove additional margin on tooltip (#6802) (651cf34)
* editor: Resolve vue 3 related console-warnings (#6779) (30484a0)
* editor: Vue3 - Fix modal positioning and multi-select tag sizing (#6783) (4e491b7)
* Email Trigger (IMAP) Node: UTF-8 attachments are not correctly named (#6856) (72814d1)
* Fix all modal sizes (#6820) (7525cfe)
* Fix horizontal overflow for dialogs (#6830) (41d8a18)
* Fix issue with key based credentials not being read correctly (#6824) (db21a8d)
* Fix tags overflow handler in workflows header (#6784) (7cd4588)
* GoToWebinar Node: Fix issue with timezone incorrectly being required (#6865) (905eef8)
* Handle subtitle errors when pasting workflow (#6826) (31a4cfc)
* Lemlist Node: Fix pagination issues with campaigns and activities (#6734) (c3e76ec)
* Linear Node: Fix issue creation priority (#6813) (fce8cc4)
* Postgres Trigger Node: Imposible to cancell execution manually (#6709) (491378d)
* Remove tag animation (#6821) (52aafe0)
* Respect set modal widths (#6771) (3aaf1ac), closes #6571
* Show NodeIcon tooltips by removing pointer-events: none (#6777) (eb898f7)
* TheHive Node: Treat ApiKey as a secret (#6786) (11a3965)
* Todoist Node: Fix issue with section id being ignored (#6799) (749468e)
* Clean up onboarding experiment (#6873) (3619345)
* core: Add metrics option to cache (#6846) (adcf5a9)
* core: Add unique id to instances (#6863) (6499f42)
* core: Create a dsl for writing db agnostic migrations (#6853) (75be1a9)
* core: Credentials for popular SecOps services, Part 1 (#6775) (11567f9)
* core: Make Redis available for backend communication (#6719) (3cad60e)
* editor: Add "Download" button if JSON data is to large (#6850) (efe08cc)
* editor: Migrate Design System and Editor UI to Vue 3 (#6476) (dd6a4c9), closes #6571
* Facebook Graph API Node: Add support for v16 and v17 (#6808) (46a41c1)
* Pipedrive Node: Add option to update the file name and description (#6883) (f8ad543)
* core: Add filtering and pagination to GET /workflows (#6845) (dceff67), closes #6876
* core: Cache roles (#6803) (e4f0418)
* core: Cache webhooks (#6825) (0511458)
* editor: Memoize locale translate calls during actions generation (#6773) (2d47e8d)

[3.2.1]
* Update n8n to 1.2.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.2.1)
* core: Don't let bull override the default redis config (#6897) (39f23ca)
* core: Update frontend urls when using the --tunnel option (#6898) (d9d2d0f)
* editor: Update execution view layout (#6882) (cdd24dc)
* Fix issue with key formatting introduced in 1.2.0 (#6896) (f21aba7)

[3.2.2]
* Update n8n to 1.2.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.2.2)
* core: Fix fetching of EE executions (#6901) (28d460a)
* core: Fix issue with key formatting if null or undefined (#6924) (493a6de)
* editor: Fix event emit on credential sharing (#6922) (dab4aa3)
* editor: Fix multiOptions parameters resetting on initial load (#6903) (ed3b89e)
* editor: Prevent workflow breaking when credential type is unknown (#6923) (92536b7)
* Email Trigger (IMAP) Node: Fix connection issue with unexpected spaces in host (#6886) (f1e7c73)
* HTTP Request Node: Improve error handling for TCP socket errors when Continue On Fail is enabled (#6925) (18e0b88)
* Respond to Webhook Node: Return headers in response (#6921) (3787156)

[3.3.0]
* Update n8n to 1.3.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.3.0)
* core: Don't let bull override the default redis config (#6897) (cfeb322)
* core: Fix fetching of EE executions (#6901) (f3fce48)
* core: Update frontend urls when using the --tunnel option (#6898) (718e613)
* editor: Disable telemetry in dev mode and in E2E tests (#6869) (808a928)
* editor: Fix code node’s content property to be reactive (#6931) (3b75bc6)
* editor: Fix event emit on credential sharing (#6922) (297c3c9)
* editor: Fix multiOptions parameters resetting on initial load (#6903) (49867c2)
* editor: Update execution view layout (#6882) (0339732)
* Email Trigger (IMAP) Node: Fix connection issue with unexpected spaces in host (#6886) (f3248e4)
* Fix issue with key formatting if null or undefined (#6924) (4e4a3cf)
* Fix issue with key formatting introduced in 1.2.0 (#6896) (0e075c9)
* Fix lag when node parameters are updated (#6941) (3eb65e0)
* HTTP Request Node: Improve error handling for TCP socket errors when Continue On Fail is enabled (#6925) (96ff1f8)
* Prevent workflow breaking when credential type is unknown (#6923) (e83b93f)
* Respond to Webhook Node: Return headers in response (#6921) (a82107f)
* core: Add support for not requiring SMTP auth with user management (#3742) (eead6d4)
* core: Descriptive message for common nodeJS errors (#6841) (3adb0b6)
* editor: Ask AI in Code node (#6672) (fde6ad1)
* Enable parallel processing on multiple queue nodes (#6295) (44afcff)

[3.3.1]
* Update n8n to 1.3.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.3.1)
* core: Fix continueOnFail for expression error in Set (#6939) (e685780)
* editor: Do not flag dynamic load options issue on expression (#6932) (81367cf)
* editor: Fix Remove all fields not removing values in resource mapper (#6940) (c80163f)
* editor: Prevent Code node linter from erroring on null parse (#6934) (239e967)

[3.4.0]
* Update n8n to 1.4.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.4.0)
* core: Add recoveryInProgress flag file (#6962) (7b96820)
* core: Fix continueOnFail for expression error in Set (#6939) (d4fac05)
* core: Fix import:workflow command (#6996) (8c38d85)
* core: Replace throw with warning when deactivating a non-active workflow (#6969) (b6a00fe)
* core: Set up OAuth2 cred test (#6960) (4fc69b7)
* editor: Do not flag dynamic load options issue on expression (#6932) (60a1ef0)
* editor: Ensure community node install button tracks user agreement (#6976) (0ddfc73)
* editor: Fix parsing for single quoted resolvables (#6982) (f32e993)

[3.4.1]
* Update n8n to 1.4.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.4.1)
* editor: Fix sending of Ask AI tracking events (#7002) (bf4f545)
* Webhook Node: Fix URL params for webhooks (#6986) (60f70f1)

[3.5.0]
* Update n8n to 1.5.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.5.0)
* Agile CRM Node: Fix issue with company address not working (#6997) (2f81652)
* Code Node: Switch over to vm2 fork (#7018) (dfe0fa6)
* core: Invalid NODES_INCLUDE should not crash the app (#7038) (04e3178), closes #6683
* core: Setup websocket keep-live messages (#6866) (8bdb07d), closes #6757
* core: Throw NodeSSLError only for nodes that allow ignoring SSL issues (#6928) (a01c3fb)
* Date & Time Node: Dont parse date if it's not set (null or undefined) (#7050) (d72f79f)
* editor: Fix sending of Ask AI tracking events (#7002) (fb05afa)
* Microsoft Excel 365 Node: Support for more extensions in workbook rlc (#7020) (d6e1cf2)
* MongoDB Node: Stringify response ObjectIDs (#6990) (9ca990b)
* MongoDB Node: Upgrade mongodb package to address CVE-2021-32050 (#7054) (d3f6356)
* Postgres Node: Empty return data fix for Postgres and MySQL (#7016) (176ccd6)
* Webhook Node: Fix URL params for webhooks (#6986) (596b569)
* core: External Secrets storage for credentials (#6477) (ed927d3)
* core: Add MFA (#4767) (2b7ba6f)
* core: Add filtering, selection and pagination to users (#6994) (b716241)
* editor: Debug executions in the editor (#6834) (c833078)
* RSS Read Node: Add support for self signed certificates (#7039) (3b9f0fe)
* Strapi Node: Add token credentials (#7048) (c01bca5)

[3.5.1]
* Add firebase-admin npm package

[3.6.0]
* Update n8n to 1.6.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.6.0)
* core: Add support for in-transit encryption (TLS) on Redis connections (#7047) (a910757)
* core: Disallow orphan executions (#7069) (8a28e98)
* core: Split event bus controller into community and ee (#7107) (011ee2e)
* editor: Standardize save text (#7093) (58b3492)
* Ensure all new executions are saved (#7061) (b8e06d2)
* Load remote resources even if expressions in non requried parameters resolve (#6987) (8a8d4e8)
* Postgres Node: Connection pool of the database object has been destroyed (#7074) (9dd5f0e)
* Postgres Node: Tunnel doesn't always close (#7087) (58e55ba)
* core: Add list query middleware to credentials (#7041) (fd78021)
* core: Add support for floating licenses (#7090) (e26553f)
* core: Migration for soft deletions for executions (#7088) (413e0bc)
* HTTP Request Node: Determine binary file name from content-disposition headers (#7032) (273d091)
* TheHive Node: Overhaul (#6457) (73e782e)

[3.6.1]
* Update n8n to 1.6.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.6.1)
* Postgres Node: Fix automatic column mapping

[3.7.0]
* Update n8n to 1.7.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.7.0)
* Code Node: Disable WASM to address CVE-2023-37903 (#7122) (36a8e91)
* Code Node: Upgrade vm2 to address CVE-2023-37466 (#7123) (0a35025)
* core: Disable Node.js custom inspection to address CVE-2023-37903 (#7125) (a223734)
* editor Account for nanoid workflow ids for subworkflow execute policy (#7094) (67092c0)
* editor: Tweak hover area of workflow / cred cards (#7108) (217de21)
* editor: Unbind workflow endpoint events in case of workspace reset (#7129) (c9b7948)
* editor: Update git repo url validation regex (#7151) (e51f173)
* Google Cloud Firestore Node: Fix empty string interpreted as number (#7136) (915cfa0)
* HubSpot Node: Fix issue with contact lists not working (#5582) (6e5a4f6)
* Postgres Node: Fix automatic column mapping (#7121) (92af131)
* Zoho CRM Node: Fix issue with Sales Order not updating (#6959) (fd800b6)
* core: Add an option to enable WAL mode for SQLite (#7118) (1d1a022)
* core: Add commands to workers to respond with current state (#7029) (7b49cf2)
* Salesforce Node: Add fax field to lead option (#7030) (01f875a)

[3.7.1]
* Update n8n to 1.7.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.7.1)
* editor: Prevent duplicate creation of credential for OAuth2 (#7163) (f6d7ffe)

[3.8.0]
* Update n8n to 1.8.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.8.0)
* core: Make parsing of content-type and content-disposition headers more flexible (#7217) (d41546b), closes #7149
* core: Resolve domains to IPv4 first (#7206) (e9ce531)
* editor: Add ssh key type selection to source control settings when regenerating key (#7172) (54bf66d)
* editor: No need to add click emitting click events, VUE delegates the handler to the root element of the component (#7182) (3c055e4)
* editor: Prevent duplicate creation of credential for OAuth2 (#7163) (07a6417)
* editor: Testing flaky resource mapper feature in e2e tests (#7165) (aaf87c3)
* HTML Node: Add pairedItem support for 'Convert to HTML Table' operation (#7196) (6bc477b)
* HTTP Request Node: Decrease default timeout to 5min (#7177) (321780d)
* seven Node: Rename sms77 to seven, fix credentials test (#7180) (cf776b8)
* X (Formerly Twitter) Node: Rename Twitter to X (keep Twitter alias) (#7179) (d317e09)
* core: Add command to trigger license refresh on workers (#7184) (9f797b9)
* core: Add rsa option to ssh key generation (#7154) (fdac2c8)
* Linear Node: Add support for OAuth2 (#7201) (12a3168)
* Microsoft Outlook Node: Node overhaul (#4449) (556a613)
* Set Node: Overhaul (#6348) (3a47455)

[3.8.1]
* Update n8n to 1.8.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.8.1)
* Airtable Node: Attachments field type fix (#7227) (ebfcc87)
* core: Handle `filename*` with quotes in Content-Disposition header (#7229) (fb36b0a)
* Ensure new Set node is on top of search list (#7215) (c8fb06f)
* Issue enforcing user limits on start plan (#7188) (50a4c5e)

[3.8.2]
* Update n8n to 1.8.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.8.2)
* editor: Add debug feature docs link (#7240) (1b2ccca)
* editor: Fix SQL editor issue (#7236) (539f954)

[3.9.0]
* Update n8n to 1.9.3
* Update base image to 4.2.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.9.3)
* editor: Fix completions for .json on quoted node name in Code node (#7382) (c8ae64b)
* editor: Revert connection snapping changes (2e43ee6)
* Notion Node: Handle empty values correctly for Notion selects + multi selects (#7383) (0c3070f)

[3.9.1]
* Fix creation of runtime dirs with new base image

[3.10.0]
* Update n8n to 1.11.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.11.1)

[3.10.1]
* Update n8n to 1.11.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.11.2)
* core: Handle gzip and deflate compressed request payloads (#7461) (f43ff71)
* core: Prevent false stalled jobs in queue mode from displaying as errored (#7435) (465a952)
* core: Reduce logging overhead for levels that do not output (#7479) (010aa57)
* editor: Allow importing the same workflow multiple times (#7458) (33e3df8), closes #7457
* editor: Fix canvas selection breaking after interacting with node actions (#7466) (90ce8de)
* editor: Fix connections disappearing after reactivating canvas and renaming a node (#7483) (b0bd0d8)
* editor: Open only one tab with plans page (#7377) (d14e9cb)
* Ldap Node: Fix issue with connections not closing correctly (#7432) (60ca02e)
* MySQL Node: Resolve expressions in v1 (#7464) (2b18909)
* TheHive 5 Node: Observable encoding in alert > create fix (#7450) (b9547ad)

[3.11.0]
* Update n8n to 1.14.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.14.2)

[3.12.0]
* Update n8n to 1.15.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.15.2)
* core: Decrease reset password token expire time (#7598) (44664d2)
* editor: Allow overriding theme from query params (#7591) (1362585)
* editor: Fix issue that frontend breaks with unkown nodes (#7596) (dd7b7d1)
* editor: Hide not supported node options (#7597) (658cacc)
* editor: Remove unknown credentials on pasting workflow (#7582) (0768271)

[3.13.0]
* Update n8n to 1.17.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.17.1)
* core: Correct permissions for getstatus (#7724) (422af2e)
* editor: Make sure LineController is registered with chart.js (#7730) (7d4f0cc)
* editor: Show v1 banner dismiss button if owner (#7722) (1f6ee4d)
* GitHub Node: Fix issue preventing file edits on branches (#7734) (340fe29)
* Google Sheets Node: Check for null before destructuring (#7729) (2507c31)

[3.14.0]
* Update n8n to 1.18.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.18.0)
* core: Ensure member and admin cannot be promoted to owner (#7830) (9b87a59)
* core: Prevent error messages due to statistics about data loading (#7824) (847f6ac)
* core: Tighten checks for multi-main setup usage (#7788) (fdb2c18)
* core: Use AbortController to notify nodes to abort execution (#6141) (d2c18c5)
* editor: Add telemetry to workflow history (#7811) (d497041)
* editor: Allow owners and admins to share workflows and credentials they don't own (#7833) (3ab3ec9)
* editor: Disable context menu actions in read-only mode (#7789) (902beff)
* editor: Fix cloud plan data loading on instance (#7841) (8b99384)
* editor: Fix credential icon for old node type version (#7843) (4074107)
* editor: Fix icon for unknown node type (#7842) (28ac5a7)
* editor: Fix mouse position in workflow previews (#7853) (c063398)
* editor: Show nice error when environment is not set up (#7778) (5835e05)
* editor: Suppress dev server websocket messages in workflow view (#7808) (685ffd7)
* Google Sheets Node: Read operation execute for each item (#7800) (d548872)
* HTTP Request Node: Enable expressions for binary input data fields (#7782) (6208af0)
* Microsoft SQL Node: Prevent double escaping table name (#7801) (73ec753)
* Add AI tool building capabilities (#7336) (87def60)
* Add initial scope checks via decorators (#7737) (a37f1cb)
* Ado 1296 spike credential setup in templates (#7786) (aae45b0)
* core: Add Support for custom CORS origins for webhooks (#7455) (99a9ea4)
* core: Allow user role modification (#7797) (7a86d36)
* core: Set up endpoint for all existing roles with license flag (#7834) (2356fb0)
* editor: Add node name and version to NDV node settings (#7731) (da85198)
* editor: Add routing middleware, permission checks, RBAC store, RBAC component (#7702) (67a8891)
* editor: Replace middleware for Role checks with Scope checks (#7847) (72852a6)
* editor: Show avatars for users currently working on the same workflow (#7763) (77bc8ec)
* Notion Node: Option to simplify output in getChildBlocks operation (#7791) (d667bca)
* Slack Node: Add support for getting the profile of a user (#7829) (90bb6ba)

[3.14.1]
* Update n8n to 1.18.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.18.1)
* core: Prevent error messages due to statistics about data loading (#7824) (c7d600a)
* core: Tighten checks for multi-main setup usage (#7788) (060987a)
* core: Use AbortController to notify nodes to abort execution (#6141) (dbad88d)
* editor: Disable context menu actions in read-only mode (#7789) (ae25503)
* editor: Fix cloud plan data loading on instance (#7841) (b0039a3)
* editor: Fix credential icon for old node type version (#7843) (9b0e2d1)
* editor: Fix icon for unknown node type (#7842) (9cd0a75)
* editor: Fix mouse position in workflow previews (#7853) (83b5e6a)
* editor: Suppress dev server websocket messages in workflow view (#7808) (8a71178)

[3.14.2]
* Update n8n to 1.18.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.18.2)
* editor: Fix deletion of last execution at execution preview (#7883) (e79bce0)

[3.15.0]
* Update n8n to 1.20.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.20.0)
* AWS DynamoDB Node: Improve error message parsing (#7793) (5ba5ed8)
* core: Allow grace period for binary data deletion after manual execution (#7889) (61d8aeb)
* core: Consolidate ownership and sharing data on workflows and credentials (#7920) (38b88b9)
* core: Fix hard deletes stopping if database query throws (#7848) (46dd4d3)
* core: Make sure mfa secret and recovery codes are not returned on login (#7936) (f5502cc)
* editor: Fix deletion of last execution at execution preview (#7883) (ce2d388)
* editor: Replace isInstanceOwner checks with scopes where applicable (#7858) (132d691)
* Google Sheets Node: Fix issue with paired items not being set correctly (#7862) (5207a2f)
* Notion Node: Fix broken Notion node parameters (#7864) (51d1f5b), closes #7791
* BambooHR Node: Add support for Only Current on company reports (#7878) (4175801)
* editor: Add sections to create node panel (#7831) (39fa8d2)
* editor: Open template credential setup from collection (#7882) (627ddb9)
* editor: Select credentials in template setup if theres only one (#7879) (fe3417a)
* editor: Improve node rendering performance when opening large workflows (#7904) (a8049a0)
* editor: Improve performance when opening large workflows with node issues (#7901) (4bd7ae2)

[3.16.0]
* Update n8n to 1.21.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.21.1)
* ActiveCampaign Node: Fix pagination issue when loading tags (#8017) (d661861)
* core: Initialize queue once in queue mode (#8025) (b1c9c50)
* core: Restore workflow ID during execution creation (#8031) (2d16161)
* editor: Add back credential use permission (#8023) (cf5c723)
* editor: Disable auto scroll and list size check when clicking on executions (#7983) (99d1771)
* editor: Show credential share info only to appropriate users (#8020) (9933fce)
* editor: Turn off executions list auto-refresh after leaving the page (#8005) (b866a94)

[3.17.0]
* Update n8n to 1.22.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.22.0)
* core: Use pinned data only for manual mode (#8164) (38a9cfb)
* Asana Node: Omit body from GET, HEAD, and DELETE requests (#8057) (094c9d7)
* core: Fix issue that pinData is not used with Test-Webhooks (#8123) (9c99075)
* editor: Fix operation change failing in certain conditions (#8114) (aaca64a)
* editor: Prevent browser zoom when scrolling inside sticky edit mode (#8116) (e5135b1)
* editor: Prevent canvas undo/redo when NDV is open (#8118) (43eca24)
* Redis Trigger Node: Activating a workflow with a Redis trigger fails (#8129) (babca25)
* core: Handle empty executions table in pruning in migrations (#8121) (6cbeb5d)
* core: Remove circular dependency in WorkflowService and ActiveWorkflowRunner (#8128) (b8e72c4)
* editor: Show public API upgrade CTA when feature is not enabled (#8109) (fd27f73)
* core: Close db connection gracefully when exiting (#8045) (e69707e)
* core: Consider timeout in shutdown an error (#8050) (4cae976)
* core: Do not display error when stopping jobless execution in queue mode (#8007) (8e6b951)
* core: Fix shutdown if terminating before hooks are initialized (#8047) (6ae2f5e)
* core: Handle multiple termination signals correctly (#8046) (67bd8ad)
* core: Initialize queue once in queue mode (#8025) (53c0b49)
* core: Prevent axios from force setting a form-urlencoded content-type (#8117) (bba9576)
* core: Remove circular references before serializing executions in public API (#8043) (989888d)
* core: Restore workflow ID during execution creation (#8031) (c5e6ba8)
* core: Use relative imports for dynamic imports in SecurityAuditService (#8086) (785bf99)
* core: Stop binary data restoration from preventing execution from finishing (#8082) (5ffff1b)
* editor: Add back credential use permission (#8023) (329e5bf)
* editor: Cleanup Executions page component (#8053) (2689c37)
* editor: Disable auto scroll and list size check when clicking on executions (#7983) (fcb8b91)
* editor: Ensure execution data overrides pinned data when copying in executions view (#8009) (1d1cb0d)
* editor: Fix copy/paste issue when switch node is in workflow (#8103) (4b86926)
* editor: Make keyboard shortcuts more strict; don't accept extra Ctrl/Alt/Shift keys (#8024) (8df49e1)
* editor: Show credential share info only to appropriate users (#8020) (b29b4d4)
* editor: Turn off executions list auto-refresh after leaving the page (#8005) (e3c363d)
* editor: Update image sizes in template description not to be full width always (#8037) (63a6e7e)
* ActiveCampaign Node: Fix pagination issue when loading tags (#8017) (1943857)
* HTTP Request Node: Do not create circular references in HTTP request node output (#8030) (5b7ea16)
* Upgrade axios to address CVE-2023-45857 (#7713) (64eb9bb)
* Add option to returnIntermediateSteps for AI agents (#8113) (7806a65)
* core: Add config option to prefer GET request over LIST when using Hashicorp Vault (#8049) (439a22d)
* core: Add N8N_GRACEFUL_SHUTDOWN_TIMEOUT env var (#8068) (614f488)
* editor: Add lead enrichment suggestions to workflow list (#8042) (36a923c)
* editor: Finalize workers view (#8052) (edfa784)
* editor: Gracefully ignore invalid payloads in postMessage handler (#8096) (9d22c7a)
* editor: Upgrade frontend tooling to address a few vulnerabilities (#8100) (19b7f1f)
* Filter Node: Overhaul UI by adding the new filter component (#8016) (3d53052)
* Respond to Webhook Node: Overhaul with improvements like returning all items (#8093) (32d397e)
* Schedule Trigger Node: Use the correct moment import (#8185) (f8307dc), closes #8184

[3.17.1]
* Update n8n to 1.22.5
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.22.5)
* Webhook Node: Fix handling of form-data files

[3.17.2]
* Update n8n to 1.22.6
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.22.6)
* HTTP Request Node: Delete response.request only when it's a valid circular references (#8293) (1de889e)
* Monday.com Node: Migrate to api 2023-10 (#8254) (4578bcd)

[3.18.0]
* Update n8n to 1.24.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.24.1)
* Add fallback resolver for langchain modules (#8308) (82199f4)
* API: Fix manual chat trigger execution (#8300) (c715ad5)
* AwsS3 Node: Return confirmation of success after upload (#8312) (d6676ba)
* core: Account for immediate confirmation request during test webhook creation (#8329) (aa14332)
* core: Prevent invalid compressed responses from making executions stuck forever (#8315) (beecfac)
* core: Replace all moment imports with moment-timezone (#8337) (65ec5cf)
* editor: Fix issue with synchronization table on LDAP not loading data (#8327) (886653a)
* editor: Properly set colors for connections and labels on nodes with pinned data (#8209) (2410047)
* Google Drive Node: Fix issue preventing service account from downloading files (#7642) (be11d38)
* HTTP Request Node: Delete response.request only when it's a valid circular references (#8293) (138df40)
* Microsoft SQL Node: Fix "Maximum call stack size exceeded" error on too many rows (#8334) (61cabb6)
* Ollama Model Node: Use a simpler credentials test (#8318) (371d0ae)
* OpenAI Node: Load correct models for operation (#8313) (6ec30d6)
* Properly output saml validation errors (#8284) (7467648)
* Supabase Node: Pagination for get all rows (#8311) (69795df)
* Venafi TLS Protect Cloud Node: Remove parameter Application Server Type (#8325) (29a19a4)
* Venafi TLS Protect Cloud Trigger Node: Handle new webhook payload format (#8326) (7680723)

[3.19.0]
* Update n8n to 1.25.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.25.1)
* AMQP Trigger Node: Properly close connection after manual test step (#8396) (ef676b7)
* Asana Node: Fix issue when connecting to the new Asana environment (#8404) (7c7a8eb)
* AWS SQS Node: Fix issue preventing data from being sent correctly (#8382) (d3f0c24)
* Change the UI text for some filter operations (#8360) (960f436)
* core: Adjust starter node priority for manual executions with pinned activators (#8386) (50d17e1)
* core: Errors are returned on the success branch if error item has other keys in addition to 'error' (#8380) (ef5c8ca)
* core: Missing pairedItem fixes (#8394) (66e0810)
* Discord Node: Remove requirement on message for webhooks (#8377) (6c51dcc)
* editor: Add pinned data for freshly added nodes (#8323) (26c6fe3)
* editor: Enable ctrl/cmd click in workflow editor header (#8387) (6aefab6)
* editor: Fix doclines for plus and minus (#8405) (2579302)
* editor: Fix invisible community package update button (#8406) (a0aae45)
* editor: Fix secondary icon for environments on sidebar menu item (#8407) (a11e7cd)
* editor: Open native context menu when editing Sticky (#8370) (e49e49f)
* editor: Use web native element in nav menus (#8385) (9584abc)
* Fix issue preventing secrets with a - in the path from being imported (#8378) (e47a0d5)
* Force posthog recording to be disabled outside cloud (#8374) (356812c)
* Microsoft SQL Node: Prevent MSSQL max parameters error by chunking (#8390) (f898982)
* Notion Node: Fix is_empty query on formula fields (#8397) (e541508)

[3.20.0]
* Update n8n to 1.26.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.26.0)
* AMQP Trigger Node: Properly close connection after manual test step (#8396) (2c14371)
* Asana Node: Fix issue when connecting to the new Asana environment (#8404) (44f6ef2)
* AWS SQS Node: Fix issue preventing data from being sent correctly (#8382) (daba5bb)
* Change the UI text for some filter operations (#8360) (976fe2e)
* core: Adjust starter node priority for manual executions with pinned activators (#8386) (749ac2b)
* core: Errors are returned on the success branch if error item has other keys in addition to 'error' (#8380) (25f51f4)
* core: Fix removal of triggers and pollers from memory on deactivation in multi-main setup (#8416) (2257ec6)
* core: Fix update workflow cli command being unable to activate all workflows (#8412) (ae06fde)
* core: Missing pairedItem fixes (#8394) (284d965)
* Discord Node: Remove requirement on message for webhooks (#8377) (c64e893)
* editor: Add pinned data for freshly added nodes (#8323) (83228e2)
* editor: Enable ctrl/cmd click in workflow editor header (#8387) (e43cf2f)
* editor: Fix copy to clipboard on insecure contexts (#8425) (7386f79)
* editor: Fix doclines for plus and minus (#8405) (ebf2b0d)
* editor: Fix invisible community package update button (#8406) (2ccb754)
* editor: Fix secondary icon for environments on sidebar menu item (#8407) (3544966)
* editor: Open native context menu when editing Sticky (#8370) (ade7d30)
* editor: Use web native element in nav menus (#8385) (e606e84)
* Fix issue preventing secrets with a - in the path from being imported (#8378) (fc94377)
* Force posthog recording to be disabled outside cloud (#8374) (f31cc07)
* Google Drive Node: Fix issue preventing upload / update working in some configurations (#8417) (4b3ea81)
* Microsoft Outlook Node: Message -> Send with attachments (#8238) (0128081)
* Microsoft SQL Node: Prevent MSSQL max parameters error by chunking (#8390) (1b0ba2c)
* Notion Node: Fix is_empty query on formula fields (#8397) (08e7db4)
* Switch Node: Fix issue preventing some regex patterns from working (#8422) (e9fea16)

[3.21.0]
* Allow custom node modules to be installed using `EXTRA_NODE_MODULES` in `/app/data/env.sh`

[3.22.0]
* Update n8n to 1.27.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.27.3)

[3.23.0]
* Update n8n to 1.29.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.29.1)

[3.23.1]
* Set the healthCheckPath to `/healthz`

[3.24.0]
* Update n8n to 1.31.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.31.1)
* AI agents, throw error on duplicate names in dynamic tools (#8766) (836bf07)
* Basic LLM Chain Node: Fix retrieving of prompt parameter for v1.3 of the node (#8817) (b5ffb7d)
* editor: Fix NDV output tabs resetting on any click (#8808) (2b06477)
* editor: Fix opening of node creator for sub-nodes connection hint link (#8809) (174dc9a)
* editor: Fix retrieving of messages from memory in chat modal (#8807) (af69dd9)
* editor: Set correct type for right input in filter component (#8771) (839793e)
* editor: Update assignment hint when user hovers table row (#8782) (5266bcb)
* Google Drive Node: Add supportsAllDrives: true to update and download (#8786) (8733832)

[3.24.1]
* symlink .ssh for environments to work

[3.24.2]
* Update n8n to 1.31.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.31.2)
* Always register webhooks on startup

[3.25.0]
* Update n8n to 1.32.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.32.2)
* editor: Improve expression editor performance by removing watchers (#8900) (788d790)

[3.26.0]
* Update n8n to 1.33.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.33.1)
* editor: Improve expression editor performance by removing watchers (#8900) (2ee4327)

[3.27.0]
* Update n8n to 1.35.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.35.0)
* core: Add missing nodeCause to paired item error (#8976) (19d9e71)
* core: Assign credential ownership correctly in source control import (#8955) (260bc07)
* editor: Fix accidental IDE code addition (#8971) (117b57c)
* editor: Make inputs in the filter component regular inputs by default (#8980) (295b650)
* editor: Show tip when user can type dot after an expression (#8931) (160dfd3)
* Fetch user cloud role and pass it on in website links (#8942) (666867a)
* Telemetry include basic llm optional promps, trigger on save workflow event (#8981) (335f363)

[3.28.0]
* Update n8n to 1.36.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.36.1)
* editor: Issue with JSON editor getting cut off (#9000) (4668db2)
* editor: Fix canvas selection for touch devices that use mouse (#9036) (286fa5c)
* editor: Fix execution debug button (#9018) (aac77e1)
* editor: Hover and active states not showing in execution list on dark mode (#9002) (bead7eb)
* editor: UI enhancements and fixes for expression inputs (#8996) (8788e2a)
* editor: Prevent chat modal opening on 'Test workflow' click (#9009) (3fd97e4)
* editor: Stop listening button not working in NDV (#9023) (02219dd)
* Add Salesforce Trigger Node (#8920) (571b613)
* Add Twilio Trigger Node (#8859) (c204995)
* core: Introduce AWS secrets manager as external secrets store (#8982) (2aab78b)
* core: Rate-limit login endpoint to mitigate brute force password guessing attacks (#9028) (a6446fe)
* Webhook Node: Overhaul (#8889) (e84c27c)
* core: Ensure status on Axios errors is available to the BE (#9015) (39002b0)
* core: Ensure only leader handles waiting executions (#9014) (52b6947)
* editor: Add fallback for expression resolution in multi-output case (#9045) (e22ef2a)
* editor: Canvas showing error toast when clicking outside of "import workflow by url" modal (#9001) (cb2e460)
* editor: Fix execution with wait node (#9051) (0fba0bf)
* editor: Issue showing Auth2 callback section when all properties are overriden (#8999) (10eea2b)
* editor: Make share modal content scrollable (#9025) (46aec96)
* editor: Make Webhook node pinnable (#9047) (8cbe2bf)
* editor: Prevent saving workflow while another save is in progress (#9048) (8fa755a)
* editor: Rerun failed nodes in manual executions (#9050) (a30e02d)
* Workflows executed from other workflows not stopping (#9010) (09e397b)

[3.28.1]
* Update n8n to 1.36.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.36.2)
* API: Accept settings.executionOrder in workflow creation (#9072) (4579fc8)
* AWS Bedrock Chat Model Node: Improve filtering of Bedrock models & fix Claude 3 (#9085) (3992ae7)
* Core: Don't revert irreversibble migrations (#9105) (a1870b3)
* Core: Ensure TTL safeguard for test webhooks applies only to multi-main setup (#9062) (89755c4)
* Core: Fix isLeader check in WaitTracker constructor (#9100) (549e8f7)
* Core: Support MySQL in MoveSshKeysToDatabase migration (#9120) (3a51593)
* Editor: Allow pinning of AI root nodes (#9060) (2489009)
* Editor: Connecting nodes to triggers when adding them together (#9042) (ae26b8f)
* Editor: Do not show overlapping trash icon in the node's settings (#9119) (5308159)
* Editor: Drop outgoing connections on order changed event for nodes with dynamic outputs (#9055) (b3adec3)
* Editor: Expand range of allowed characters in expressions (#9083) (e947f39)
* Editor: Fix displaying logic of execution retry button (#9061) (c3a5ed1)
* Editor: Fix issue with case insensitive tags (#9071) (9a9b49a)
* Editor: Fix issues in dark mode (#9068) (c101ec8)
* Editor: Open links from embedded chat in new tab (#9121) (252dc97)
* Editor: Render dates correctly in parameter hint (#9089) (0e88b89)
* Editor: UX improvements to mfa setup modal (#9059) (f8d825e)
* HTTP Request Node: Duplicate key names support for form data (#9040) (3fb532d)
* MySQL Node: Query Parameters parse string to number (#9011) (f50d4db)
* Respond to Webhook Node: Continue on fail and error branch support (#9115) (dedd5c1)
* Summarization Chain Node: 'Final Prompt to Combine' and 'Individual Summary Prompt' options (#8391) (e4c05e4)

[3.29.0]
* Update n8n to 1.37.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.37.3)
* editor: Add object keys that need bracket access to autocomplete (#9088) (98bcd50)
* GitHub Node: Add option to get pull requests (#9094) (4d9000b)
* Google Gemini Chat Model Node: Add support for new Google Gemini models (#9130) (f1215cd)
* Summarize Node: Option to continue when field to summarize can't be found in any items (#9118) (d7abc30)
* core: Don't create multiple owners when importing credentials or workflows (#9112) (32db869)
* core: Exclude oAuth callback urls from browser-id checks (#9158) (58b6a9d)
* core: Improve browserId checks, and add logging (#9161) (cff50fb)

[3.30.0]
* Update n8n to 1.38.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.38.1)
* core: Exclude oAuth callback urls from browser-id checks (#9158) (c1d07fb)
* core: Improve browserId checks, and add logging (#9161) (e16d18c)
* editor: Fix parameter reset on credential change in Discord node (#9137) (336344f)
* Postgres Node: Convert js arrays to postgres type, if column type is ARRAY (#9160) (ee0c685)
* Schedule Trigger Node: Default to 0 minute if falsy on hourly run (#9146) (3d6455e)

[3.30.1]
* Update n8n to 1.38.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.38.2)
* core: Stop relying on filesystem for SSH keys (#9217) (988526c)

[3.31.0]
* Update n8n to 1.39.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.39.1)
* core: Fix browser session refreshes not working (#9212) (a67246c)
* core: Stop relying on filesystem for SSH keys (#9217) (3418dfb)
* Discord Node: When using OAuth2 authentication, check if user is a guild member when sending direct message (#9183) (d0250b2)
* Google Drive Node: Create from text operation (#9185) (44bcc03)
* MySQL Node: Query to statements splitting fix (#9207) (67c92dc)

[3.32.0]
* Update n8n to 1.40.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.40.0)

[3.33.0]
* Update n8n to 1.41.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.41.0)
* Cast boolean values in filter parameter (#9260) (30c8efc)
* core: Prevent occassional 429s on license init in multi-main setup (#9284) (22b6f90)
* core: Report missing SAML attributes early with an actionable error message (#9316) (225fdbb)
* core: Webhooks responding with binary data should not prematurely end the response stream (#9063) (23b676d)
* editor: Fix multi-select parameters with load options getting cleared (#9324) (0ee4b6c)
* editor: Fix shortcut issue on save buttons (#9309) (e74c14f)
* editor: Resolve $vars and $secrets in expressions in credentials fields (#9289) (d92f994)
* editor: Show MFA section to instance owner, even when external auth is enabled (#9301) (b65e0e2)
* Gmail Node: Remove duplicate options when creating drafts (#9299) (bfb0eb7)
* Linear Node: Fix issue with data not always being returned (#9273) (435272b)
* n8n Form Trigger Node: Fix missing options when using respond to webhook (#9282) (6ab3781)
* Pipedrive Node: Improve type-safety in custom-property handling (#9319) (c8895c5)
* Read PDF Node: Disable JS evaluation from PDFs (#9336) (c4bf5b2)
* editor: Implement AI Assistant chat UI (#9300) (491c6ec)
* editor: Temporarily disable AI error helper (#9329) (35b983b)
* LinkedIn Node: Upgrade LinkedIn API version (#9307) (3860077)
* Redis Node: Add support for TLS (#9266) (0a2de09)
* Send Email Node: Add an option to customize client host-name on SMTP connections (#9322) (d0d52de)
* Slack Node: Update to use the new API method for file uploads (#9323) (695e762)

[3.33.1]
* Update n8n to 1.41.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.41.1)
* Code Node: Bind helper methods to the correct context (#9380) (a515686)
* core: Add an option to disable STARTTLS for SMTP connections (#9415) (bc1e6f9)
* editor: Fix blank Public API page (#9409) (d090a7a)
* Email Trigger (IMAP) Node: Handle attachments correctly (#9410) (5633eec)
* Mattermost Node: Fix issue when fetching reactions (#9375) (c19d4aa)

[3.34.0]
* Update n8n to 1.42.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.42.1)
* Code Node: Bind helper methods to the correct context (#9380) (82c8801)
* Cortex Node: Fix issue with analyzer response not working for file observables (#9374) (ed22dcd)
* editor: Render backticks as code segments in error view (#9352) (4ed5850)
* Mattermost Node: Fix issue when fetching reactions (#9375) (78e7c7a)
* AI Agent Node: Implement Tool calling agent (#9339) (677f534)
* core: Allow using a custom certificates in docker containers (#8705) (6059722)
* core: Node hints(warnings) system (#8954) (da6088d)
* core: Node version available in expression (#9350) (a00467c)
* editor: Add examples for number & boolean, add new methods (#9358) (7b45dc3)
* editor: Add examples for object and array expression methods (#9360) (5293663)
* editor: Add item selector to expression output (#9281) (dc5994b)
* editor: Autocomplete info box: improve structure and add examples (#9019) (c92c870)
* editor: Remove AI Error Debugging (#9337) (cda062b)
* Slack Node: Add block support for message updates (#8925) (1081429)

[3.35.0]
* Update n8n to 1.44.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.44.1)

[3.36.0]
* Update n8n to 1.45.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.45.1)

[3.37.0]
* Update n8n to 1.46.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.46.0)

[3.38.0]
* Update n8n to 1.47.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.47.1)

[3.38.1]
* Update n8n to 1.47.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.47.2)

[3.38.2]
* Update n8n to 1.47.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.47.3)

[3.39.0]
* Update n8n to 1.48.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.48.3)

[3.40.0]
* Update n8n to 1.49.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.49.0)

[3.41.0]
* Update n8n to 1.50.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.50.1)

[3.41.1]
* Update n8n to 1.50.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.50.2)

[3.42.0]
* Update n8n to 1.51.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.51.1)

[3.42.1]
* Update n8n to 1.51.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.51.2)

[3.43.0]
* Update n8n to 1.52.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.52.2)

[3.44.0]
* Update n8n to 1.53.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.53.1)

[3.44.1]
* Update n8n to 1.53.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.53.2)

[3.44.2]
* Update n8n to 1.54.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.54.2)

[3.44.3]
* Update n8n to 1.54.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.54.4)

[3.45.0]
* Update n8n to 1.55.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.55.3)

[3.46.0]
* Update n8n to 1.56.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.56.2)

[3.47.0]
* Update n8n to 1.57.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.57.0)

[3.48.0]
* Update n8n to 1.58.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.58.1)

[3.48.1]
* Update n8n to 1.58.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.58.2)

[3.49.0]
* Update n8n to 1.59.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.59.3)

[3.49.1]
* Update n8n to 1.59.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.59.4)

[3.50.0]
* Update n8n to 1.60.1
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.60.1)

[3.51.0]
* Update n8n to 1.61.0
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.61.0)

[3.52.0]
* Update n8n to 1.62.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.62.3)

[3.52.1]
* Update n8n to 1.62.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.62.4)

[3.52.2]
* Update n8n to 1.62.5
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.62.5)

[3.52.3]
* Update n8n to 1.62.6
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.62.6)

[3.52.4]
* Update n8n to 1.63.4
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.63.4)

[3.53.0]
* Update n8n to 1.64.2
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.2)

[3.53.1]
* Update n8n to 1.64.3
* [Full changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)


[3.54.0]
* Update n8n to 1.65.2
* **editor:** Support middle click to scroll when using a mouse on new canvas ([#&#8203;11384](https://github.com/n8n-io/n8n/issues/11384)) ([436b783](https://github.com/n8n-io/n8n/commit/436b783bd2d6200d318ac70dba95ccda9148b261))
* **n8n Form Node:** Form Trigger does not wait in multi-form mode ([#&#8203;11404](https://github.com/n8n-io/n8n/issues/11404)) ([d57da55](https://github.com/n8n-io/n8n/commit/d57da557a03d88ba9ee554b869dcacb47ff0b1be))
* **core:** Make execution and its data creation atomic ([#&#8203;11392](https://github.com/n8n-io/n8n/issues/11392)) ([480987e](https://github.com/n8n-io/n8n/commit/480987e4abb62c5534c9f2dfb392c3640c85fa47))
* **core:** On unhandled rejections, extract the original exception correctly ([#&#8203;11389](https://github.com/n8n-io/n8n/issues/11389)) ([343eb53](https://github.com/n8n-io/n8n/commit/343eb531169d48d1d245ab00da50a643d097fd7a))
* **AI Agent Node:** Preserve `intermediateSteps` when using output parser with non-tool agent ([#&#8203;11363](https://github.com/n8n-io/n8n/issues/11363)) ([e61a853](https://github.com/n8n-io/n8n/commit/e61a8535aa39653b9a87575ea911a65318282167))
* **API:** `PUT /credentials/:id` should move the specified credential, not the first one in the database ([#&#8203;11365](https://github.com/n8n-io/n8n/issues/11365)) ([e6b2f8e](https://github.com/n8n-io/n8n/commit/e6b2f8e7e6ebbb6e3776a976297d519e99ac6c64))
* **API:** Correct credential schema for response in `POST /credentials` ([#&#8203;11340](https://github.com/n8n-io/n8n/issues/11340)) ([f495875](https://github.com/n8n-io/n8n/commit/f4958756b4976e0b608b9155dab84564f7e8804e))
* **core:** Account for waiting jobs during shutdown ([#&#8203;11338](https://github.com/n8n-io/n8n/issues/11338)) ([c863abd](https://github.com/n8n-io/n8n/commit/c863abd08300b53ea898fc4d06aae97dec7afa9b))
* **core:** Add missing primary key to execution annotation tags table ([#&#8203;11168](https://github.com/n8n-io/n8n/issues/11168)) ([b4b543d](https://github.com/n8n-io/n8n/commit/b4b543d41daa07753eca24ab93bf7445f672361d))
* **core:** Change dedupe value column type from varchar(255) to text ([#&#8203;11357](https://github.com/n8n-io/n8n/issues/11357)) ([7a71cff](https://github.com/n8n-io/n8n/commit/7a71cff4d75fe4e7282a398b4843428e0161ba8c))
* **core:** Do not debounce webhooks, triggers and pollers activation ([#&#8203;11306](https://github.com/n8n-io/n8n/issues/11306)) ([64bddf8](https://github.com/n8n-io/n8n/commit/64bddf86536ddd688638a643d24f80c947a12f31))
* **core:** Enforce nodejs version consistently ([#&#8203;11323](https://github.com/n8n-io/n8n/issues/11323)) ([0fa2e8c](https://github.com/n8n-io/n8n/commit/0fa2e8ca85005362d9043d82469f3c3525f4c4ef))
* **core:** Fix memory issue with empty model response ([#&#8203;11300](https://github.com/n8n-io/n8n/issues/11300)) ([216b119](https://github.com/n8n-io/n8n/commit/216b119350949de70f15cf2d61f474770803ad7a))
* **core:** Fix race condition when resolving post-execute promise ([#&#8203;11360](https://github.com/n8n-io/n8n/issues/11360)) ([4f1816e](https://github.com/n8n-io/n8n/commit/4f1816e03db00219bc2e723e3048848aef7f8fe1))
* **core:** Sanitise IdP provided information in SAML test pages ([#&#8203;11171](https://github.com/n8n-io/n8n/issues/11171)) ([74fc388](https://github.com/n8n-io/n8n/commit/74fc3889b946e8f224e65ef8d3d44125404aa4fc))
* Don't show pin button in input panel when there's binary data ([#&#8203;11267](https://github.com/n8n-io/n8n/issues/11267)) ([c0b5b92](https://github.com/n8n-io/n8n/commit/c0b5b92f62a2d7ba60492eb27daced268b654fe9))
* **editor:** Add Personal project to main navigation ([#&#8203;11161](https://github.com/n8n-io/n8n/issues/11161)) ([1f441f9](https://github.com/n8n-io/n8n/commit/1f441f97528f58e905eaf8930577bbcd08debf06))
* **editor:** Fix Cannot read properties of undefined (reading 'finished') ([#&#8203;11367](https://github.com/n8n-io/n8n/issues/11367)) ([475d72e](https://github.com/n8n-io/n8n/commit/475d72e0bc9e13c6dc56129902f6f89c67547f78))
* **editor:** Fix delete all existing executions ([#&#8203;11352](https://github.com/n8n-io/n8n/issues/11352)) ([3ec103f](https://github.com/n8n-io/n8n/commit/3ec103f8baaa89e579844947d945f00bec9e498e))
* **editor:** Fix pin data button disappearing after reload ([#&#8203;11198](https://github.com/n8n-io/n8n/issues/11198)) ([3b2f63e](https://github.com/n8n-io/n8n/commit/3b2f63e248cd0cba04087e2f40e13d670073707d))
* **editor:** Fix RunData non-binary pagination when binary data is present ([#&#8203;11309](https://github.com/n8n-io/n8n/issues/11309)) ([901888d](https://github.com/n8n-io/n8n/commit/901888d5b1027098653540c72f787f176941f35a))
* **editor:** Fix sorting problem in older browsers that don't support `toSorted` ([#&#8203;11204](https://github.com/n8n-io/n8n/issues/11204)) ([c728a2f](https://github.com/n8n-io/n8n/commit/c728a2ffe01f510a237979a54897c4680a407800))
* **editor:** Follow-up fixes to projects side menu ([#&#8203;11327](https://github.com/n8n-io/n8n/issues/11327)) ([4dde772](https://github.com/n8n-io/n8n/commit/4dde772814c55e66efcc9b369ae443328af21b14))
* **editor:** Keep always focus on the first item on the node's search panel ([#&#8203;11193](https://github.com/n8n-io/n8n/issues/11193)) ([c57cac9](https://github.com/n8n-io/n8n/commit/c57cac9e4d447c3a4240a565f9f2de8aa3b7c513))
* **editor:** Open Community+ enrollment modal only for the instance owner ([#&#8203;11292](https://github.com/n8n-io/n8n/issues/11292)) ([76724c3](https://github.com/n8n-io/n8n/commit/76724c3be6e001792433045c2b2aac0ef16d4b8a))
* **editor:** Record sessionStarted telemetry event in Setting Store ([#&#8203;11334](https://github.com/n8n-io/n8n/issues/11334)) ([1b734dd](https://github.com/n8n-io/n8n/commit/1b734dd9f42885594ce02400cfb395a4f5e7e088))
* Ensure NDV params don't get cut off early and scrolled to the top ([#&#8203;11252](https://github.com/n8n-io/n8n/issues/11252)) ([054fe97](https://github.com/n8n-io/n8n/commit/054fe9745ff6864f9088aa4cd66ed9e7869520d5))
* **HTTP Request Tool Node:** Fix the undefined response issue when authentication is enabled ([#&#8203;11343](https://github.com/n8n-io/n8n/issues/11343)) ([094ec68](https://github.com/n8n-io/n8n/commit/094ec68d4c00848013aa4eec4ac5efbd2c92afc5))
* Include error in the message in JS task runner sandbox ([#&#8203;11359](https://github.com/n8n-io/n8n/issues/11359)) ([0708b3a](https://github.com/n8n-io/n8n/commit/0708b3a1f8097af829c92fe106ea6ba375d6c500))
* **Microsoft SQL Node:** Fix execute query to allow for non select query to run ([#&#8203;11335](https://github.com/n8n-io/n8n/issues/11335)) ([ba158b4](https://github.com/n8n-io/n8n/commit/ba158b4f8533bd3430db8766d4921f75db5c1a11))
* **OpenAI Chat Model Node, Ollama Chat Model Node:** Change default model to a more up-to-date option ([#&#8203;11293](https://github.com/n8n-io/n8n/issues/11293)) ([0be04c6](https://github.com/n8n-io/n8n/commit/0be04c6348d8c059a96c3d37a6d6cd587bfb97f3))
* **Pinecone Vector Store Node:** Prevent populating of vectors after manually stopping the execution ([#&#8203;11288](https://github.com/n8n-io/n8n/issues/11288)) ([fbae17d](https://github.com/n8n-io/n8n/commit/fbae17d8fb35a5197fa183e3639bb36762dc73d2))
* **Postgres Node:** Special datetime values cause errors ([#&#8203;11225](https://github.com/n8n-io/n8n/issues/11225)) ([3c57f46](https://github.com/n8n-io/n8n/commit/3c57f46aaeb968d2974f2dc9790317a6a6fab624))
* Resend invite operation on users list ([#&#8203;11351](https://github.com/n8n-io/n8n/issues/11351)) ([e4218de](https://github.com/n8n-io/n8n/commit/e4218debd18812fa3aa508339afd3de03c4d69dc))
* **SSH Node:** Cleanup temporary binary files as soon as possible ([#&#8203;11305](https://github.com/n8n-io/n8n/issues/11305)) ([08a7b5b](https://github.com/n8n-io/n8n/commit/08a7b5b7425663ec6593114921c2e22ab37d039e))
* Add report bug buttons ([#&#8203;11304](https://github.com/n8n-io/n8n/issues/11304)) ([296f68f](https://github.com/n8n-io/n8n/commit/296f68f041b93fd32ac7be2b53c2b41d58c2998a))
* **AI Agent Node:** Make tools optional when using OpenAI model with Tools agent ([#&#8203;11212](https://github.com/n8n-io/n8n/issues/11212)) ([fed7c3e](https://github.com/n8n-io/n8n/commit/fed7c3ec1fb0553adaa9a933f91aabfd54fe83a3))
* **core:**  introduce JWT API keys for the public API ([#&#8203;11005](https://github.com/n8n-io/n8n/issues/11005)) ([679fa4a](https://github.com/n8n-io/n8n/commit/679fa4a10a85fc96e12ca66fe12cdb32368bc12b))
* **core:** Enforce config file permissions on startup ([#&#8203;11328](https://github.com/n8n-io/n8n/issues/11328)) ([c078a51](https://github.com/n8n-io/n8n/commit/c078a516bec857831cc904ef807d0791b889f3a2))
* **core:** Handle cycles in workflows when partially executing them ([#&#8203;11187](https://github.com/n8n-io/n8n/issues/11187)) ([321d6de](https://github.com/n8n-io/n8n/commit/321d6deef18806d88d97afef2f2c6f29e739ccb4))
* **editor:** Separate node output execution tooltip from status icon ([#&#8203;11196](https://github.com/n8n-io/n8n/issues/11196)) ([cd15e95](https://github.com/n8n-io/n8n/commit/cd15e959c7af82a7d8c682e94add2b2640624a70))
* **GitHub Node:** Add workflow resource operations ([#&#8203;10744](https://github.com/n8n-io/n8n/issues/10744)) ([d309112](https://github.com/n8n-io/n8n/commit/d3091126472faa2c8f270650e54027d19dc56bb6))
* **n8n Form Page Node:** New node ([#&#8203;10390](https://github.com/n8n-io/n8n/issues/10390)) ([643d66c](https://github.com/n8n-io/n8n/commit/643d66c0ae084a0d93dac652703adc0a32cab8de))
* **n8n Google My Business Node:** New node ([#&#8203;10504](https://github.com/n8n-io/n8n/issues/10504)) ([bf28fbe](https://github.com/n8n-io/n8n/commit/bf28fbefe5e8ba648cba1555a2d396b75ee32bbb))
* Run `mfa.beforeSetup` hook before enabling MFA ([#&#8203;11116](https://github.com/n8n-io/n8n/issues/11116)) ([25c1c32](https://github.com/n8n-io/n8n/commit/25c1c3218cf1075ca3abd961236f3b2fbd9d6ba9))
* **Structured Output Parser Node:** Refactor Output Parsers and Improve Error Handling ([#&#8203;11148](https://github.com/n8n-io/n8n/issues/11148)) ([45274f2](https://github.com/n8n-io/n8n/commit/45274f2e7f081e194e330e1c9e6a5c26fca0b141))
[3.55.0]
* Update n8n to 1.66.0
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **Asana Node:** Fix issue with pagination ([#&#8203;11415](https://github.com/n8n-io/n8n/issues/11415)) ([04c075a](https://github.com/n8n-io/n8n/commit/04c075a46bcc7b1964397f0244b0fde99476212d))
* **core:** Add 'user_id' to `license-community-plus-registered` telemetry event ([#&#8203;11430](https://github.com/n8n-io/n8n/issues/11430)) ([7a8dafe](https://github.com/n8n-io/n8n/commit/7a8dafe9902fbc0d5001c50579c34959b95211ab))
* **core:** Add safeguard for command publishing ([#&#8203;11337](https://github.com/n8n-io/n8n/issues/11337)) ([656439e](https://github.com/n8n-io/n8n/commit/656439e87138f9f96dea5a683cfdac3f661ffefb))
* **core:** Ensure `LoggerProxy` is not scoped ([#&#8203;11379](https://github.com/n8n-io/n8n/issues/11379)) ([f4ea943](https://github.com/n8n-io/n8n/commit/f4ea943c9cb2321e41705de6c5c27535a0f5eae0))
* **core:** Ensure `remove-triggers-and-pollers` command is not debounced ([#&#8203;11486](https://github.com/n8n-io/n8n/issues/11486)) ([529d4fc](https://github.com/n8n-io/n8n/commit/529d4fc3ef5206bd1b02d27634342cc50b45997e))
* **core:** Ensure job processor does not reprocess amended executions ([#&#8203;11438](https://github.com/n8n-io/n8n/issues/11438)) ([c152a3a](https://github.com/n8n-io/n8n/commit/c152a3ac56f140a39eea4771a94f5a3082118df7))
* **core:** Fix Message Event Bus Metrics not counting up for labeled metrics ([#&#8203;11396](https://github.com/n8n-io/n8n/issues/11396)) ([7fc3b25](https://github.com/n8n-io/n8n/commit/7fc3b25d21c6c4f1802f34b1ae065a65cac3001b))
* **core:** Fix resolving of $fromAI expression via `evaluateExpression` ([#&#8203;11397](https://github.com/n8n-io/n8n/issues/11397)) ([2e64464](https://github.com/n8n-io/n8n/commit/2e6446454defbd3e5a47b66e6fd46d4f1b9fbd0f))
* **core:** Make execution and its data creation atomic ([#&#8203;11392](https://github.com/n8n-io/n8n/issues/11392)) ([ed30d43](https://github.com/n8n-io/n8n/commit/ed30d43236bf3c6b657022636a02a41be01aa152))
* **core:** On unhandled rejections, extract the original exception correctly ([#&#8203;11389](https://github.com/n8n-io/n8n/issues/11389)) ([8608bae](https://github.com/n8n-io/n8n/commit/8608baeb7ec302daddc8adca6e39778dcf7b2eda))
* **editor:**  Fix TypeError: Cannot read properties of undefined (reading '0') ([#&#8203;11399](https://github.com/n8n-io/n8n/issues/11399)) ([ae37c52](https://github.com/n8n-io/n8n/commit/ae37c520a91c75e353e818944b36a3619c0d8b4a))
* **editor:** Add Retry button for AI Assistant errors ([#&#8203;11345](https://github.com/n8n-io/n8n/issues/11345)) ([7699240](https://github.com/n8n-io/n8n/commit/7699240073122cdef31cf109fd37fa66961f588a))
* **editor:** Change tooltip for workflow with execute workflow trigger ([#&#8203;11374](https://github.com/n8n-io/n8n/issues/11374)) ([dcd6038](https://github.com/n8n-io/n8n/commit/dcd6038c3085135803cdaa546a239359a6d449eb))
* **editor:** Ensure toasts show above modal overlays ([#&#8203;11410](https://github.com/n8n-io/n8n/issues/11410)) ([351134f](https://github.com/n8n-io/n8n/commit/351134f786af933f5f310bf8d9897269387635a0))
* **editor:** Fit view consistently after nodes are initialized on new canvas ([#&#8203;11457](https://github.com/n8n-io/n8n/issues/11457)) ([497d637](https://github.com/n8n-io/n8n/commit/497d637fc5308b9c4a06bc764152fde1f1a9c130))
* **editor:** Fix adding connections when initializing workspace in templates view on new canvas ([#&#8203;11451](https://github.com/n8n-io/n8n/issues/11451)) ([ea47b02](https://github.com/n8n-io/n8n/commit/ea47b025fb16c967d4fc73dcacc6e260d2aecd61))
* **editor:** Fix rendering of AI logs ([#&#8203;11450](https://github.com/n8n-io/n8n/issues/11450)) ([73b0a80](https://github.com/n8n-io/n8n/commit/73b0a80ac92b4f4b5a300d0ec1c833b4395a222a))
* **editor:** Hide data mapping tooltip in credential edit modal ([#&#8203;11356](https://github.com/n8n-io/n8n/issues/11356)) ([ff14dcb](https://github.com/n8n-io/n8n/commit/ff14dcb3a1ddaea4eca7c1ecd2e92c0abb0c413c))
* **editor:** Prevent running workflow that has issues if listening to webhook ([#&#8203;11402](https://github.com/n8n-io/n8n/issues/11402)) ([8b0a48f](https://github.com/n8n-io/n8n/commit/8b0a48f53010378e497e4cc362fda75a958cf363))
* **editor:** Run external hooks after settings have been initialized ([#&#8203;11423](https://github.com/n8n-io/n8n/issues/11423)) ([0ab24c8](https://github.com/n8n-io/n8n/commit/0ab24c814abd1787268750ba808993ab2735ac52))
* **editor:** Support middle click to scroll when using a mouse on new canvas ([#&#8203;11384](https://github.com/n8n-io/n8n/issues/11384)) ([46f3b4a](https://github.com/n8n-io/n8n/commit/46f3b4a258f89f02e0d2bd1eef25a22e3a721167))
* **HTTP Request Tool Node:** Fix HTML response optimization issue ([#&#8203;11439](https://github.com/n8n-io/n8n/issues/11439)) ([cf37e94](https://github.com/n8n-io/n8n/commit/cf37e94dd875e9f6ab1f189146fb34e7296af93c))
* **n8n Form Node:** Form Trigger does not wait in multi-form mode ([#&#8203;11404](https://github.com/n8n-io/n8n/issues/11404)) ([151f4dd](https://github.com/n8n-io/n8n/commit/151f4dd7b8f800af424f8ae64cb8238975fb3cb8))
* Update required node js version in CONTRIBUTING.md ([#&#8203;11437](https://github.com/n8n-io/n8n/issues/11437)) ([4f511aa](https://github.com/n8n-io/n8n/commit/4f511aab68651caa8fe47f70cd7cdb88bb06a3e2))
* **Anthropic Chat Model Node:** Add model claude-3-5-sonnet-20241022 ([#&#8203;11465](https://github.com/n8n-io/n8n/issues/11465)) ([f6c8890](https://github.com/n8n-io/n8n/commit/f6c8890a8069de221b9b96e735418ecc9624cf7b))
* **core:** Handle nodes with multiple inputs and connections during partial executions ([#&#8203;11376](https://github.com/n8n-io/n8n/issues/11376)) ([cb7c4d2](https://github.com/n8n-io/n8n/commit/cb7c4d29a6f042b590822e5b9c67fff0a8f0863d))
* **editor:** Add descriptive header to projects /workflow ([#&#8203;11203](https://github.com/n8n-io/n8n/issues/11203)) ([5d19e8f](https://github.com/n8n-io/n8n/commit/5d19e8f2b45dc1abc5a8253f9e3a0fdacb1ebd91))
* **editor:** Improve placeholder for vector store tool ([#&#8203;11483](https://github.com/n8n-io/n8n/issues/11483)) ([629e092](https://github.com/n8n-io/n8n/commit/629e09240785bc648ff6575f97910fbb4e77cdab))
* **editor:** Remove edge execution animation on new canvas ([#&#8203;11446](https://github.com/n8n-io/n8n/issues/11446)) ([a701d87](https://github.com/n8n-io/n8n/commit/a701d87f5ba94ffc811e424b60e188b26ac6c1c5))
* **editor:** Update ownership pills ([#&#8203;11155](https://github.com/n8n-io/n8n/issues/11155)) ([8147038](https://github.com/n8n-io/n8n/commit/8147038cf87dca657602e617e49698065bf1a63f))

[3.56.0]
* Update n8n to 1.67.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **core:** Revert all the context helpers changes ([#&#8203;11616](https://github.com/n8n-io/n8n/issues/11616)) ([8a804b3](https://github.com/n8n-io/n8n/commit/8a804b3b49412e1cf82a7462d8a6dd5e29fb78d4))
* Bring back nodes panel telemetry events ([#&#8203;11456](https://github.com/n8n-io/n8n/issues/11456)) ([130c942](https://github.com/n8n-io/n8n/commit/130c942f633788d1b2f937d6fea342d4450c6e3d))
* **core:** Account for double quotes in instance base URL ([#&#8203;11495](https://github.com/n8n-io/n8n/issues/11495)) ([c5191e6](https://github.com/n8n-io/n8n/commit/c5191e697a9a9ebfa2b67587cd01b5835ebf6ea8))
* **core:** Do not delete waiting executions when saving of successful executions is disabled ([#&#8203;11458](https://github.com/n8n-io/n8n/issues/11458)) ([e8757e5](https://github.com/n8n-io/n8n/commit/e8757e58f69e091ac3d2a2f8e8c8e33ac57c1e47))

[3.56.1]
* Set the release channel to `stable`

[3.57.0]
* Update n8n to 1.68.0
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **AI Agent Node:** Throw better errors for non-tool agents when using structured tools ([#&#8203;11582](https://github.com/n8n-io/n8n/issues/11582)) ([9b6123d](https://github.com/n8n-io/n8n/commit/9b6123dfb2648f880c7829211fa07666611ad0ea))
* **Auto-fixing Output Parser Node:** Only run retry chain on parsing errors ([#&#8203;11569](https://github.com/n8n-io/n8n/issues/11569)) ([21b31e4](https://github.com/n8n-io/n8n/commit/21b31e488ff6ab0bcf3c79edcd17b9e37d4c64a4))
* **core:** Continue with error output reverse items in success branch ([#&#8203;11684](https://github.com/n8n-io/n8n/issues/11684)) ([6d5ee83](https://github.com/n8n-io/n8n/commit/6d5ee832966fab96043b0d65697c059ced61d334))
* **core:** Ensure task runner server closes websocket connection correctly ([#&#8203;11633](https://github.com/n8n-io/n8n/issues/11633)) ([b496bf3](https://github.com/n8n-io/n8n/commit/b496bf3147d2cd873d24371be02cb7ea5dbd8621))
* **core:** Handle websocket connection error more gracefully in task runners ([#&#8203;11635](https://github.com/n8n-io/n8n/issues/11635)) ([af7d6e6](https://github.com/n8n-io/n8n/commit/af7d6e68d0436ff8a3d4e8410dc8ee4f3a035c44))
* **core:** Improve model sub-nodes error handling ([#&#8203;11418](https://github.com/n8n-io/n8n/issues/11418)) ([57467d0](https://github.com/n8n-io/n8n/commit/57467d0285d67509322630c4c01130022f274a41))
* **core:** Make push work for waiting webhooks ([#&#8203;11678](https://github.com/n8n-io/n8n/issues/11678)) ([600479b](https://github.com/n8n-io/n8n/commit/600479bf36ba8870d4aecacad19a2dc5f2d97959))
* **core:** Revert all the context helpers changes ([#&#8203;11616](https://github.com/n8n-io/n8n/issues/11616)) ([20fd38f](https://github.com/n8n-io/n8n/commit/20fd38f3517f7ef35604ba16abb4d951270b4d50))
* **core:** Set the authentication methad to `email` during startup if the SAML configuration in the database has been corrupted ([#&#8203;11600](https://github.com/n8n-io/n8n/issues/11600)) ([6439291](https://github.com/n8n-io/n8n/commit/6439291738dec16261979d6d835acbc63743d51a))
* **core:** Use cached value in retrieval of personal project owner ([#&#8203;11533](https://github.com/n8n-io/n8n/issues/11533)) ([04029d8](https://github.com/n8n-io/n8n/commit/04029d82a11b52990890380ba7094055b18e7c1f))
* Credentials save button is hidden unless you make changes to the ([#&#8203;11492](https://github.com/n8n-io/n8n/issues/11492)) ([835fbfe](https://github.com/n8n-io/n8n/commit/835fbfe337dd8dc0d0b0318c7227e174484e1328))
* **editor:** Add stickies to node insert position conflict check allowlist ([#&#8203;11624](https://github.com/n8n-io/n8n/issues/11624)) ([fc39e3c](https://github.com/n8n-io/n8n/commit/fc39e3ca16231c176957e2504d55df6b416874fe))
* **editor:** Adjust Scrollbar Width of RunData Header Row ([#&#8203;11561](https://github.com/n8n-io/n8n/issues/11561)) ([d17d76a](https://github.com/n8n-io/n8n/commit/d17d76a85d5425bc091d29fc84605ffbccbca984))
* **editor:** Cap NDV Output View Tab Index to prevent rare edge case ([#&#8203;11614](https://github.com/n8n-io/n8n/issues/11614)) ([a6c8ee4](https://github.com/n8n-io/n8n/commit/a6c8ee4a82e6055766dc1307f79c774c17bb5f4d))
* **editor:** Do not show hover tooltip when autocomplete is active ([#&#8203;11653](https://github.com/n8n-io/n8n/issues/11653)) ([23caf43](https://github.com/n8n-io/n8n/commit/23caf43e30342a21d45c825f438aa1e6193601d1))
* **editor:** Enable pinning main output with error and always allow unpinning ([#&#8203;11452](https://github.com/n8n-io/n8n/issues/11452)) ([40c8882](https://github.com/n8n-io/n8n/commit/40c88822acdcda6401bd92b9cf89d013c44b8453))
* **editor:** Fix collapsing nested items in expression modal schema view ([#&#8203;11645](https://github.com/n8n-io/n8n/issues/11645)) ([41dea52](https://github.com/n8n-io/n8n/commit/41dea522fbfb1c9acee51f47f384973914454b5f))
* **editor:** Fix default workflow settings ([#&#8203;11632](https://github.com/n8n-io/n8n/issues/11632)) ([658568e](https://github.com/n8n-io/n8n/commit/658568e2700bfd5b61da53f3052403d0098c2d90))
* **editor:** Fix duplicate chat trigger ([#&#8203;11693](https://github.com/n8n-io/n8n/issues/11693)) ([a025848](https://github.com/n8n-io/n8n/commit/a025848ec4be96f74d4de2ab104256b6d89bb837))
* **editor:** Fix hiding SQL query output when trying to select ([#&#8203;11649](https://github.com/n8n-io/n8n/issues/11649)) ([4dbf2f4](https://github.com/n8n-io/n8n/commit/4dbf2f4256111985b367030020f1494b8a8b95af))
* **editor:** Fix scrolling in code edit modal ([#&#8203;11647](https://github.com/n8n-io/n8n/issues/11647)) ([8f695f3](https://github.com/n8n-io/n8n/commit/8f695f3417820e7b9bb04b78972f6abbd61abbe8))
* **editor:** Prevent error being thrown in RLC while loading ([#&#8203;11676](https://github.com/n8n-io/n8n/issues/11676)) ([ca8cb45](https://github.com/n8n-io/n8n/commit/ca8cb455ba59831295c238afb11aeab6ad18428e))
* **editor:** Prevent NodeCreator from swallowing AskAssistant enter event ([#&#8203;11532](https://github.com/n8n-io/n8n/issues/11532)) ([db94f16](https://github.com/n8n-io/n8n/commit/db94f169fcd03983fc78a3b4c5e11543610825bf))
* **editor:** Show node executing status shortly before switching to success on new canvas ([#&#8203;11675](https://github.com/n8n-io/n8n/issues/11675)) ([b0ba24c](https://github.com/n8n-io/n8n/commit/b0ba24cbbc55cebc26e9f62ead7396c4c8fbd062))
* **editor:** Show only error title and 'Open errored node' button; hide 'Ask Assistant' in root for sub-node errors ([#&#8203;11573](https://github.com/n8n-io/n8n/issues/11573)) ([8cba100](https://github.com/n8n-io/n8n/commit/8cba1004888f60ca653ee069501c13b3cadcc561))
* **Facebook Lead Ads Trigger Node:** Fix issue with optional fields ([#&#8203;11692](https://github.com/n8n-io/n8n/issues/11692)) ([70d315b](https://github.com/n8n-io/n8n/commit/70d315b3d5b8f5f3e0f39527bba11e254a52028e))
* **Google BigQuery Node:** Add item index to insert error ([#&#8203;11702](https://github.com/n8n-io/n8n/issues/11702)) ([145d092](https://github.com/n8n-io/n8n/commit/145d0921b217bbd4b625beaacfa14429560bf51b))
* **Google Drive Node:** Fix file upload for streams ([#&#8203;11698](https://github.com/n8n-io/n8n/issues/11698)) ([770230f](https://github.com/n8n-io/n8n/commit/770230fbfe0b9e86527254e201c4602fbced94ff))
* **In-Memory Vector Store Node:** Fix displaying execution data of connected embedding nodes ([#&#8203;11701](https://github.com/n8n-io/n8n/issues/11701)) ([40ade15](https://github.com/n8n-io/n8n/commit/40ade151724f4af28a6ed959fd9363450ea711fd))
* **Item List Output Parser Node:** Fix number of items parameter issue ([#&#8203;11696](https://github.com/n8n-io/n8n/issues/11696)) ([01ebe9d](https://github.com/n8n-io/n8n/commit/01ebe9dd38629afbab954fb489f3ef2bb7ab5b34))
* **n8n Form Node:** Find completion page ([#&#8203;11674](https://github.com/n8n-io/n8n/issues/11674)) ([ed3ad6d](https://github.com/n8n-io/n8n/commit/ed3ad6d684597f7c4b7419dfa81d476e66f10eba))
* **n8n Form Node:** Open form page if form trigger has pin data ([#&#8203;11673](https://github.com/n8n-io/n8n/issues/11673)) ([f0492bd](https://github.com/n8n-io/n8n/commit/f0492bd3bb0d94802a2707fb1cf861313b6ea808))
* **n8n Form Node:** Trigger page stack in waiting if error in workflow ([#&#8203;11671](https://github.com/n8n-io/n8n/issues/11671)) ([94b5873](https://github.com/n8n-io/n8n/commit/94b5873248212a5500f02cf3c0d74df6f9d8fb26))
* **n8n Form Trigger Node:** Checkboxes different sizes ([#&#8203;11677](https://github.com/n8n-io/n8n/issues/11677)) ([c08d23c](https://github.com/n8n-io/n8n/commit/c08d23c00f01bb6fcb3b75f02e0338af375f9b32))
* NDV search bugs ([#&#8203;11613](https://github.com/n8n-io/n8n/issues/11613)) ([c32cf64](https://github.com/n8n-io/n8n/commit/c32cf644a6b8c21558e802449329877845de70b1))
* **Notion Node:** Extract page url ([#&#8203;11643](https://github.com/n8n-io/n8n/issues/11643)) ([cbdd535](https://github.com/n8n-io/n8n/commit/cbdd535fe0cb4e032ea82f008dcf35cc5f2264c2))
* **Redis Chat Memory Node:** Respect the SSL flag from the credential ([#&#8203;11689](https://github.com/n8n-io/n8n/issues/11689)) ([b5cbf75](https://github.com/n8n-io/n8n/commit/b5cbf7566d351d8a8e9972f13ff5867ff1c8d7d0))
* **Supabase Node:** Reset query parameters in get many operation ([#&#8203;11630](https://github.com/n8n-io/n8n/issues/11630)) ([7458229](https://github.com/n8n-io/n8n/commit/74582290c04d2dd32300b1a6c7715862ae837d34))
* **Switch Node:** Maintain output connections ([#&#8203;11162](https://github.com/n8n-io/n8n/issues/11162)) ([9bd79fc](https://github.com/n8n-io/n8n/commit/9bd79fceebc4453d0fe40ae5f628d5e31ff2b326))
* **AI Transform Node:** Show warning for binary data ([#&#8203;11560](https://github.com/n8n-io/n8n/issues/11560)) ([ddbb263](https://github.com/n8n-io/n8n/commit/ddbb263dce0fc458abc95d850217251bb49d2b83))
* **core:** Make all http requests made with `httpRequestWithAuthentication` abortable ([#&#8203;11704](https://github.com/n8n-io/n8n/issues/11704)) ([0d8aada](https://github.com/n8n-io/n8n/commit/0d8aada49005d6a6078e8460003a0de61a8f423c))
* **editor:** Improve how we show default Agent prompt and Memory session parameters ([#&#8203;11491](https://github.com/n8n-io/n8n/issues/11491)) ([565f8cd](https://github.com/n8n-io/n8n/commit/565f8cd8c78b534a50e272997d659d162fa86625))
* **editor:** Improve workflow loading performance on new canvas ([#&#8203;11629](https://github.com/n8n-io/n8n/issues/11629)) ([f1e2df7](https://github.com/n8n-io/n8n/commit/f1e2df7d0753aa0f33cf299100a063bf89cc8b35))
* **editor:** Redesign Canvas Chat ([#&#8203;11634](https://github.com/n8n-io/n8n/issues/11634)) ([a412ab7](https://github.com/n8n-io/n8n/commit/a412ab7ebfcd6aa9051a8ca36e34f1067102c998))
* **editor:** Restrict when a ChatTrigger Node is added automatically ([#&#8203;11523](https://github.com/n8n-io/n8n/issues/11523)) ([93a6f85](https://github.com/n8n-io/n8n/commit/93a6f858fa3eb53f8b48b2a3d6b7377279dd6ed1))
* Github star button in-app ([#&#8203;11695](https://github.com/n8n-io/n8n/issues/11695)) ([0fd684d](https://github.com/n8n-io/n8n/commit/0fd684d90c830f8b0aab12b7f78a1fa5619c62c9))
* **Oura Node:** Update node for v2 api ([#&#8203;11604](https://github.com/n8n-io/n8n/issues/11604)) ([3348fbb](https://github.com/n8n-io/n8n/commit/3348fbb1547c430ff8707b298640e3461d3f6536))
* **editor:** Add lint rules for optimization-friendly syntax ([#&#8203;11681](https://github.com/n8n-io/n8n/issues/11681)) ([88295c7](https://github.com/n8n-io/n8n/commit/88295c70495ae3d017674d5745972a346fcbaf12))

[3.57.1]
* Update n8n to 1.68.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **core:** Fix broken execution query when using projectId ([#&#8203;11852](https://github.com/n8n-io/n8n/issues/11852)) ([0ef9c3c](https://github.com/n8n-io/n8n/commit/0ef9c3c6846e63a691e5b60a6f829ac14b4873f2))
* **editor:** Fix AI assistant loading message layout ([#&#8203;11819](https://github.com/n8n-io/n8n/issues/11819)) ([830f68a](https://github.com/n8n-io/n8n/commit/830f68a96004418a8c79a6f710712b94d94fa0af))
* **editor:** Restore workers view ([#&#8203;11876](https://github.com/n8n-io/n8n/issues/11876)) ([66403ce](https://github.com/n8n-io/n8n/commit/66403cefc53750b0992220b3b14654bf1b52e16d))

[3.58.0]
* Update n8n to 1.69.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **editor:** Restore workers view ([#&#8203;11876](https://github.com/n8n-io/n8n/issues/11876)) ([c28ed67](https://github.com/n8n-io/n8n/commit/c28ed67b1c9e5407e661b109371c89556db4e2e5))
* **core:** Bring back execution data on the `executionFinished` push message ([#&#8203;11821](https://github.com/n8n-io/n8n/issues/11821)) ([55ae09a](https://github.com/n8n-io/n8n/commit/55ae09af786aff206964f08bf4c6b2399d7719be))
* **core:** Fix broken execution query when using projectId ([#&#8203;11852](https://github.com/n8n-io/n8n/issues/11852)) ([74261da](https://github.com/n8n-io/n8n/commit/74261dadeca81d314e33563f4c63a1beeb0b094b))
* **editor:** Fix AI assistant loading message layout ([#&#8203;11819](https://github.com/n8n-io/n8n/issues/11819)) ([5f98dd4](https://github.com/n8n-io/n8n/commit/5f98dd477395aada0d62369aa394984549b2f454))
* Add supported versions warning to Zep memory node ([#&#8203;11803](https://github.com/n8n-io/n8n/issues/11803)) ([9cc5bc1](https://github.com/n8n-io/n8n/commit/9cc5bc1aef974fe6c2511c1597b90c8b54ba6b9c))
* **AI Agent Node:** Escape curly brackets in tools description for non Tool agents ([#&#8203;11772](https://github.com/n8n-io/n8n/issues/11772)) ([83abdfa](https://github.com/n8n-io/n8n/commit/83abdfaf027a0533824a3ac3e4bab3cad971821a))
* **Anthropic Chat Model Node:** Update credentials test endpoint ([#&#8203;11756](https://github.com/n8n-io/n8n/issues/11756)) ([6cf0aba](https://github.com/n8n-io/n8n/commit/6cf0abab5bcddb407571271b9f174e66bb209790))
* **core:** Add missing env vars to task runner config ([#&#8203;11810](https://github.com/n8n-io/n8n/issues/11810)) ([870c576](https://github.com/n8n-io/n8n/commit/870c576ed9d7ce4ef005db9c8bedd78e91084c9c))
* **core:** Allow Azure's SAML metadata XML containing WS-Federation nodes to pass validation ([#&#8203;11724](https://github.com/n8n-io/n8n/issues/11724)) ([3b62bd5](https://github.com/n8n-io/n8n/commit/3b62bd58c264be0225a74ae0eb35c4761c419b79))
* **core:** Delete binary data parent folder when pruning executions ([#&#8203;11790](https://github.com/n8n-io/n8n/issues/11790)) ([17ef2c6](https://github.com/n8n-io/n8n/commit/17ef2c63f69b811bdd28006df3b6edd446837971))
* **core:** Fix `diagnostics.enabled` default value ([#&#8203;11809](https://github.com/n8n-io/n8n/issues/11809)) ([5fa72b0](https://github.com/n8n-io/n8n/commit/5fa72b0512b00bdc6a1065b7b604c9640f469454))
* **core:** Improve the security on OAuth callback endpoints  ([#&#8203;11593](https://github.com/n8n-io/n8n/issues/11593)) ([274fcf4](https://github.com/n8n-io/n8n/commit/274fcf45d393d8db1d2fb5ae1e774a4c9198a178))
* **core:** Restore old names for pruning config keys ([#&#8203;11782](https://github.com/n8n-io/n8n/issues/11782)) ([d15b8d0](https://github.com/n8n-io/n8n/commit/d15b8d05092d2ed9dd45fcfa34b4177f60469ebd))
* **core:** Unload any existing version of a community nodes package before upgrading it ([#&#8203;11727](https://github.com/n8n-io/n8n/issues/11727)) ([1d8fd13](https://github.com/n8n-io/n8n/commit/1d8fd13d841b73466ba5f8044d17d7199da7e856))
* **editor:** Add documentation link to insufficient quota message ([#&#8203;11777](https://github.com/n8n-io/n8n/issues/11777)) ([1987363](https://github.com/n8n-io/n8n/commit/1987363f7941285c51fda849a4ac92832368b25a))
* **editor:** Add project header subtitle ([#&#8203;11797](https://github.com/n8n-io/n8n/issues/11797)) ([ff4261c](https://github.com/n8n-io/n8n/commit/ff4261c16845c7de1790fdf0eaa9f57b37822289))
* **editor:** Change Home label to Overview ([#&#8203;11736](https://github.com/n8n-io/n8n/issues/11736)) ([1a78360](https://github.com/n8n-io/n8n/commit/1a783606b4ef22d85e173a2a780d5c49ff208932))
* **editor:** Fix executions sorting ([#&#8203;11808](https://github.com/n8n-io/n8n/issues/11808)) ([cd5ad65](https://github.com/n8n-io/n8n/commit/cd5ad65e90a3be4d67b51521772e0fceb7f4abc7))
* **editor:** Fix partial executions not working due to broken push message queue and race conditions ([#&#8203;11798](https://github.com/n8n-io/n8n/issues/11798)) ([b05d435](https://github.com/n8n-io/n8n/commit/b05d43519994abdd34a65462d14184c779d0b667))
* **editor:** Fix reordered switch connections when copying nodes on new canvas ([#&#8203;11788](https://github.com/n8n-io/n8n/issues/11788)) ([6c2dad7](https://github.com/n8n-io/n8n/commit/6c2dad79143f5b0c255ab8c97c3255314834c458))
* **editor:** Fix the issue with RMC Values to Send collection disappears  ([#&#8203;11710](https://github.com/n8n-io/n8n/issues/11710)) ([7bb9002](https://github.com/n8n-io/n8n/commit/7bb9002cbc10cf58550f53a30c6fd7151f8e7355))
* **editor:** Improve formatting of expired trial error message ([#&#8203;11708](https://github.com/n8n-io/n8n/issues/11708)) ([8a0ad0f](https://github.com/n8n-io/n8n/commit/8a0ad0f910feeada6d0c63e81c3e97a1a6e44de7))
* **editor:** Optimize application layout ([#&#8203;11769](https://github.com/n8n-io/n8n/issues/11769)) ([91f9390](https://github.com/n8n-io/n8n/commit/91f9390b90a68d064ea00d10505bf3767ddec1d4))
* **Google Sheets Trigger Node:** Fix issue with regex showing correct sheet as invalid ([#&#8203;11770](https://github.com/n8n-io/n8n/issues/11770)) ([d5ba1a0](https://github.com/n8n-io/n8n/commit/d5ba1a059b7a67154f17f8ad3fcfe66c5c031059))
* **HTTP Request Node:** Continue using error ([#&#8203;11733](https://github.com/n8n-io/n8n/issues/11733)) ([d1bae1a](https://github.com/n8n-io/n8n/commit/d1bae1ace062dd5b64087e0313e78599b5994355))
* **n8n Form Node:** Support expressions in completion page ([#&#8203;11781](https://github.com/n8n-io/n8n/issues/11781)) ([1099167](https://github.com/n8n-io/n8n/commit/10991675fe2e6913e8f03d565b670257941f18e5))
* Prevent workflow to run if active and single webhook service ([#&#8203;11752](https://github.com/n8n-io/n8n/issues/11752)) ([bcb9a20](https://github.com/n8n-io/n8n/commit/bcb9a2078186ff80e03ca3b8532d3585c307d86b))
* **Read/Write Files from Disk Node:** Escape parenthesis when reading file ([#&#8203;11753](https://github.com/n8n-io/n8n/issues/11753)) ([285534e](https://github.com/n8n-io/n8n/commit/285534e6d0ceb60290bd0a928054e494252148fe))
* **YouTube Node:** Issue in published before and after dates filters ([#&#8203;11741](https://github.com/n8n-io/n8n/issues/11741)) ([7381c28](https://github.com/n8n-io/n8n/commit/7381c28af00148b329690021b921267a48a6eaa3))
* **core:** Improve debugging of sub-workflows ([#&#8203;11602](https://github.com/n8n-io/n8n/issues/11602)) ([fd3254d](https://github.com/n8n-io/n8n/commit/fd3254d5874a03b57421246b77a519787536a93e))
* **core:** Improve handling of manual executions with wait nodes ([#&#8203;11750](https://github.com/n8n-io/n8n/issues/11750)) ([61696c3](https://github.com/n8n-io/n8n/commit/61696c3db313cdc97925af728ff5c68415f9b6b2))
* **editor:** Add Info Note to NDV Output Panel if no existing Tools were used during Execution ([#&#8203;11672](https://github.com/n8n-io/n8n/issues/11672)) ([de0e861](https://github.com/n8n-io/n8n/commit/de0e86150f4d0615481e5ec3869465cfd1ce822f))
* **editor:** Add option to create sub workflow from workflows list in `Execute Workflow` node ([#&#8203;11706](https://github.com/n8n-io/n8n/issues/11706)) ([c265d44](https://github.com/n8n-io/n8n/commit/c265d44841eb147115563ce24c56666b1e321433))
* **editor:** Add selection navigation using the keyboard on new canvas ([#&#8203;11679](https://github.com/n8n-io/n8n/issues/11679)) ([6cd9b99](https://github.com/n8n-io/n8n/commit/6cd9b996af0406caf65941503276524de9e2add4))
* **editor:** Add universal Create Resource Menu ([#&#8203;11564](https://github.com/n8n-io/n8n/issues/11564)) ([b38ce14](https://github.com/n8n-io/n8n/commit/b38ce14ec94d74aa1c9780a0572804ff6266588d))
* **Embeddings Azure OpenAI Node, Azure OpenAI Chat Model Node:** Add support for basePath url in Azure Open AI nodes ([#&#8203;11784](https://github.com/n8n-io/n8n/issues/11784)) ([e298ebe](https://github.com/n8n-io/n8n/commit/e298ebe90d69f466ee897855472eaa7be1d96aba))
* **Embeddings OpenAI Node, Embeddings Azure OpenAI Node:** Add dimensions option ([#&#8203;11773](https://github.com/n8n-io/n8n/issues/11773)) ([de01a8a](https://github.com/n8n-io/n8n/commit/de01a8a01d37f33ab8bcbc65588cafebda969922))
* GitHub stars dismiss button ([#&#8203;11794](https://github.com/n8n-io/n8n/issues/11794)) ([8fbad74](https://github.com/n8n-io/n8n/commit/8fbad74ab685c2ba0395c30cee0ddf9498fb8984))

[3.59.0]
* Update n8n to 1.70.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **core:** Opt-out from optimizations if `$item` is used ([#&#8203;12036](https://github.com/n8n-io/n8n/issues/12036)) ([dfa1806](https://github.com/n8n-io/n8n/commit/dfa1806fd40b17081e4315abaaa934ab068d1036))
* **core:** Use the configured timezone in task runner ([#&#8203;12032](https://github.com/n8n-io/n8n/issues/12032)) ([b0f6c6f](https://github.com/n8n-io/n8n/commit/b0f6c6f0648a89ab4565274ed37cad7225cbc6da))
* **editor:** Fix bug causing connection lines to disappear when hovering stickies  ([#&#8203;11950](https://github.com/n8n-io/n8n/issues/11950)) ([c018b63](https://github.com/n8n-io/n8n/commit/c018b63eea7bfc5478ffa2f9ab79379716cabb60))
* **editor:** Fix pin data showing up in production executions on new canvas ([#&#8203;11951](https://github.com/n8n-io/n8n/issues/11951)) ([e4e5077](https://github.com/n8n-io/n8n/commit/e4e5077ddcdbc2af90decf6d6e8e2b64b0a38370))
* **AI Agent Node:** Add binary message before scratchpad to prevent tool calling loops ([#&#8203;11845](https://github.com/n8n-io/n8n/issues/11845)) ([5c80cb5](https://github.com/n8n-io/n8n/commit/5c80cb57cf709a1097a38e0394aad6fce5330eba))
* CodeNodeEditor walk  cannot read properties of null ([#&#8203;11129](https://github.com/n8n-io/n8n/issues/11129)) ([d99e0a7](https://github.com/n8n-io/n8n/commit/d99e0a7c979a1ee96b2eea1b9011d5bce375289a))
* **core:** Bring back execution data on the `executionFinished` push message ([#&#8203;11821](https://github.com/n8n-io/n8n/issues/11821)) ([0313570](https://github.com/n8n-io/n8n/commit/03135702f18e750ba44840dccfec042270629a2b))
* **core:** Correct invalid WS status code on removing connection ([#&#8203;11901](https://github.com/n8n-io/n8n/issues/11901)) ([1d80225](https://github.com/n8n-io/n8n/commit/1d80225d26ba01f78934a455acdcca7b83be7205))
* **core:** Don't use unbound context methods in code sandboxes ([#&#8203;11914](https://github.com/n8n-io/n8n/issues/11914)) ([f6c0d04](https://github.com/n8n-io/n8n/commit/f6c0d045e9683cd04ee849f37b96697097c5b41d))
* **core:** Fix broken execution query when using projectId ([#&#8203;11852](https://github.com/n8n-io/n8n/issues/11852)) ([a061dbc](https://github.com/n8n-io/n8n/commit/a061dbca07ad686c563e85c56081bc1a7830259b))
* **core:** Fix validation of items returned in the task runner ([#&#8203;11897](https://github.com/n8n-io/n8n/issues/11897)) ([a535e88](https://github.com/n8n-io/n8n/commit/a535e88f1aec8fbbf2eb9397d38748f49773de2d))
* **editor:** Add missing trigger waiting tooltip on new canvas ([#&#8203;11918](https://github.com/n8n-io/n8n/issues/11918)) ([a8df221](https://github.com/n8n-io/n8n/commit/a8df221bfbb5428d93d03f539bcfdaf29ee20c21))
* **editor:** Don't re-render input panel after node finishes executing ([#&#8203;11813](https://github.com/n8n-io/n8n/issues/11813)) ([b3a99a2](https://github.com/n8n-io/n8n/commit/b3a99a2351079c37ed6d83f43920ba80f3832234))
* **editor:** Fix AI assistant loading message layout ([#&#8203;11819](https://github.com/n8n-io/n8n/issues/11819)) ([89b4807](https://github.com/n8n-io/n8n/commit/89b48072432753137b498c338af7777036fdde7a))
* **editor:** Fix new canvas discovery tooltip position after adding github stars button ([#&#8203;11898](https://github.com/n8n-io/n8n/issues/11898)) ([f4ab5c7](https://github.com/n8n-io/n8n/commit/f4ab5c7b9244b8fdde427c12c1a152fbaaba0c34))
* **editor:** Fix node position not getting set when dragging selection on new canvas ([#&#8203;11871](https://github.com/n8n-io/n8n/issues/11871)) ([595de81](https://github.com/n8n-io/n8n/commit/595de81c03b3e48

[3.59.1]
* Update n8n to 1.70.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **editor:** Don't reset all Parameter Inputs when switched to read-only  ([#&#8203;12063](https://github.com/n8n-io/n8n/issues/12063)) ([86b80e4](https://github.com/n8n-io/n8n/commit/86b80e4900bc404238e4c81a8334cd14a51b845f))
* **editor:** Fix Nodeview.v2 reinitialise based on route changes ([#&#8203;12062](https://github.com/n8n-io/n8n/issues/12062)) ([92e2773](https://github.com/n8n-io/n8n/commit/92e27737b3d7246b9cbd39c54e8ea4aa271224c5))
* **editor:** Fix switching from v2 to v1 ([#&#8203;12050](https://github.com/n8n-io/n8n/issues/12050)) ([deaab8b](https://github.com/n8n-io/n8n/commit/deaab8b8d99be7b520327ff1dd9df5dd743b258a))
* **editor:** Load node types in demo and preview modes ([#&#8203;12048](https://github.com/n8n-io/n8n/issues/12048)) ([104d56e](https://github.com/n8n-io/n8n/commit/104d56e7b19e89f4371b0fe5db96ca1c4dda81d4))
* **editor:** Polyfill crypto.randomUUID ([#&#8203;12052](https://github.com/n8n-io/n8n/issues/12052)) ([6f4ea58](https://github.com/n8n-io/n8n/commit/6f4ea58ef666ce91e6471bf6fadea1b08a1ce68d))

[3.59.2]
* Update n8n to 1.70.4
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.64.3)
* **core:** Make sure task runner exits ([#&#8203;12123](https://github.com/n8n-io/n8n/issues/12123)) ([996c9e9](https://github.com/n8n-io/n8n/commit/996c9e9e9c9f20a1c6bfc38cc48a9c6209fcfcf0))
* **core:** Cancel runner task on timeout in external mode ([#&#8203;12101](https://github.com/n8n-io/n8n/issues/12101)) ([881a77d](https://github.com/n8n-io/n8n/commit/881a77d5617167f83fa541478b624aa68b9523bf))

[3.60.0]
* Update n8n to 1.71.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.71.2)
* core: Make sure task runner exits (#12123) (78315ac)
* editor: Fix svg background pattern rendering on safari (#12079) (36eb25c)
* core: Cancel runner task on timeout in external mode (#12101) (f18263b)

[3.60.1]
* Update n8n to 1.71.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.71.3)
* **core:** Fix `$getWorkflowStaticData` on task runners ([#&#8203;12153](https://github.com/n8n-io/n8n/issues/12153)) ([26435f7](https://github.com/n8n-io/n8n/commit/26435f74bf5029f45424564989f141c682105002))
* **editor:** Fix canvas panning using `Control` + `Left Mouse Button` on Windows ([#&#8203;12104](https://github.com/n8n-io/n8n/issues/12104)) ([77caa64](https://github.com/n8n-io/n8n/commit/77caa64179b3ad12904fc4dc3532ed6b705d8326))

[3.61.0]
* Update n8n to 1.72.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.72.1)
* **core:** Fix `$getWorkflowStaticData` on task runners ([#&#8203;12153](https://github.com/n8n-io/n8n/issues/12153)) ([215cb22](https://github.com/n8n-io/n8n/commit/215cb221319c9545899ff6fdc88d3d640db24754))
* **core:** Fix race condition in AI tool invocation with multiple items from the parent ([#&#8203;12169](https://github.com/n8n-io/n8n/issues/12169)) ([67a99dd](https://github.com/n8n-io/n8n/commit/67a99ddce085e4b80018b93d0161a5468f82079a))
* **editor:** Fix canvas panning using `Control` + `Left Mouse Button` on Windows ([#&#8203;12104](https://github.com/n8n-io/n8n/issues/12104)) ([ef3358d](https://github.com/n8n-io/n8n/commit/ef3358da4b5eb0fd9c44acea5cafb7b23bbaa22e))
* Allow disabling MFA with recovery codes ([#&#8203;12014](https://github.com/n8n-io/n8n/issues/12014)) ([95d56fe](https://github.com/n8n-io/n8n/commit/95d56fee8d0168b75fca6dcf41702d2f10c930a8))
* Chat triggers don't work with the new partial execution flow ([#&#8203;11952](https://github.com/n8n-io/n8n/issues/11952)) ([2b6a72f](https://github.com/n8n-io/n8n/commit/2b6a72f1289c01145edf2b88e5027d2b9b2ed624))
* **core:** Execute nodes after loops correctly with the new partial execution flow ([#&#8203;11978](https://github.com/n8n-io/n8n/issues/11978)) ([891dd7f](https://github.com/n8n-io/n8n/commit/891dd7f995c78a2355a049b7ced981a5f6b1c40c))
* **core:** Fix support for multiple invocation of AI tools ([#&#8203;12141](https://github.com/n8n-io/n8n/issues/12141)) ([c572c06](https://github.com/n8n-io/n8n/commit/c572c0648ca5b644b222157b3cabac9c05704a84))
* **core:** Make sure task runner exits ([#&#8203;12123](https://github.com/n8n-io/n8n/issues/12123)) ([c5effca](https://github.com/n8n-io/n8n/commit/c5effca7d47a713f157eea21d7892002e9ab7283))
* **core:** Remove run data of nodes unrelated to the current partial execution ([#&#8203;12099](https://github.com/n8n-io/n8n/issues/12099)) ([c4e4d37](https://github.com/n8n-io/n8n/commit/c4e4d37a8785d1a4bcd376cb1c49b82a80aa4391))
* **core:** Return homeProject when filtering workflows by project id ([#&#8203;12077](https://github.com/n8n-io/n8n/issues/12077)) ([efafeed](https://github.com/n8n-io/n8n/commit/efafeed33482100a23fa0163a53b9ce93cd6b2c3))
* **editor:** Don't reset all Parameter Inputs when switched to read-only  ([#&#8203;12063](https://github.com/n8n-io/n8n/issues/12063)) ([706702d](https://github.com/n8n-io/n8n/commit/706702dff8da3c2e949e2c98dd5b34b299a1f17c))
* **editor:** Fix canvas panning using `Control` + `Left Mouse Button` on Windows ([#&#8203;12104](https://github.com/n8n-io/n8n/issues/12104)) ([43009b6](https://github.com/n8n-io/n8n/commit/43009b6aa820f24b9e6f519e7a45592aa21db03e))
* **editor:** Fix Nodeview.v2 reinitialise based on route changes ([#&#8203;12062](https://github.com/n8n-io/n8n/issues/12062)) ([b1f8663](https://github.com/n8n-io/n8n/commit/b1f866326574974eb2936e6b02771346e83e7137))
* **editor:** Fix svg background pattern rendering on safari ([#&#8203;12079](https://github.com/n8n-io/n8n/issues/12079)) ([596f221](https://github.com/n8n-io/n8n/commit/596f22103c01e14063ebb2388c4dabf4714d37c6))
* **editor:** Fix switching from v2 to v1 ([#&#8203;12050](https://github.com/n8n-io/n8n/issues/12050)) ([5c76de3](https://github.com/n8n-io/n8n/commit/5c76de324c2e25b0d8b74cdab79f04aa616d8c4f))
* **editor:** Improvements to the commit modal ([#&#8203;12031](https://github.com/n8n-io/n8n/issues/12031)) ([4fe1952](https://github.com/n8n-io/n8n/commit/4fe1952e2fb3379d95da42a7bb531851af6d0094))
* **editor:** Load node types in demo and preview modes ([#&#8203;12048](https://github.com/n8n-io/n8n/issues/12048)) ([4ac5f95](https://github.com/n8n-io/n8n/commit/4ac5f9527bbec382a65ed3f1d9c41d6948c154e3))
* **editor:** Polyfill crypto.randomUUID ([#&#8203;12052](https://github.com/n8n-io/n8n/issues/12052)) ([0537524](https://github.com/n8n-io/n8n/commit/0537524c3e45d7633415c7a9175a3857ad52cd58))
* **editor:** Redirect Settings to the proper sub page depending on the instance type (cloud or not) ([#&#8203;12053](https://github.com/n8n-io/n8n/issues/12053)) ([a16d006](https://github.com/n8n-io/n8n/commit/a16d006f893cac927d674fa447b08c1205b67c54))
* **editor:** Render sanitized HTML content in toast messages ([#&#8203;12139](https://github.com/n8n-io/n8n/issues/12139)) ([0468945](https://github.com/n8n-io/n8n/commit/0468945c99f083577c4cc71f671b4b950f6aeb86))
* **editor:** Universal button snags ([#&#8203;11974](https://github.com/n8n-io/n8n/issues/11974)) ([956b11a](https://github.com/n8n-io/n8n/commit/956b11a560528336a74be40f722fa05bf3cca94d))
* **editor:** Update concurrency UI considering different types of instances ([#&#8203;12068](https://github.com/n8n-io/n8n/issues/12068)) ([fa572bb](https://github.com/n8n-io/n8n/commit/fa572bbca4397b1cc42668530497444630ed17eb))
* **FTP Node:** Fix issue with creating folders on rename ([#&#8203;9340](https://github.com/n8n-io/n8n/issues/9340)) ([eb7d593](https://github.com/n8n-io/n8n/commit/eb7d5934ef8bc6e999d6de4c0b8025ce175df5dd))
* **n8n Form Node:** Completion page display if EXECUTIONS_DATA_SAVE_ON_SUCCESS=none ([#&#8203;11869](https://github.com/n8n-io/n8n/issues/11869)) ([f4c2523](https://github.com/n8n-io/n8n/commit/f4c252341985fe03927a2fd5d60ba846ec3dfc77))
* **OpenAI Node:** Allow updating assistant files ([#&#8203;12042](https://github.com/n8n-io/n8n/issues/12042)) ([7b20f8a](https://github.com/n8n-io/n8n/commit/7b20f8aaa8befd19dbad0af3bf1b881342c1fca5))
* **AI Transform Node:** Reduce payload size ([#&#8203;11965](https://github.com/n8n-io/n8n/issues/11965)) ([d8ca8de](https://github.com/n8n-io/n8n/commit/d8ca8de13a4cbb856696873bdb56c66b12a5b027))
* **core:** Add option to filter for empty variables ([#&#8203;12112](https://github.com/n8n-io/n8n/issues/12112)) ([a63f0e8](https://github.com/n8n-io/n8n/commit/a63f0e878e21da9924451e2679939209b34b6583))
* **core:** Cancel runner task on timeout in external mode ([#&#8203;12101](https://github.com/n8n-io/n8n/issues/12101)) ([addb4fa](https://github.com/n8n-io/n8n/commit/addb4fa352c88d856e463bb2b7001173c4fd6a7d))
* **core:** Parent workflows should wait for sub-workflows to finish ([#&#8203;11985](https://github.com/n8n-io/n8n/issues/11985)) ([60b3dcc](https://github.com/n8n-io/n8n/commit/60b3dccf9317da6f3013be35a78ce21d0416ad80))
* **editor:** Implementing the `Easy AI Workflow` experiment ([#&#8203;12043](https://github.com/n8n-io/n8n/issues/12043)) ([67ed1d2](https://github.com/n8n-io/n8n/commit/67ed1d2c3c2e69d5a96daf7de2795c02f5d8f15b))
* **Redis Node:** Add support for continue on fail / error output branch ([#&#8203;11714](https://github.com/n8n-io/n8n/issues/11714)) ([ed35958](https://github.com/n8n-io/n8n/commit/ed359586c88a7662f4d94d58c5a87cf91d027ab9))

[3.62.0]
* Update n8n to 1.73.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.73.1)
* **OpenAI Node:** Add quotes to default base URL ([#&#8203;12312](https://github.com/n8n-io/n8n/issues/12312)) ([22ba3d2](https://github.com/n8n-io/n8n/commit/22ba3d21fceb1592ffc303caaf04c871eba509ff))
* **core:** Ensure runners do not throw on unsupported console methods ([#&#8203;12167](https://github.com/n8n-io/n8n/issues/12167)) ([57c6a61](https://github.com/n8n-io/n8n/commit/57c6a6167dd2b30f0082a416daefce994ecad33a))
* **core:** Fix `$getWorkflowStaticData` on task runners ([#&#8203;12153](https://github.com/n8n-io/n8n/issues/12153)) ([b479f14](https://github.com/n8n-io/n8n/commit/b479f14ef5012551b823bea5d2ffbddedfd50a77))

[3.63.0]
* Update n8n to 1.74.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.74.1)
* **editor:** Fix parameter input validation ([#&#8203;12532](https://github.com/n8n-io/n8n/issues/12532)) ([6f757f1](https://github.com/n8n-io/n8n/commit/6f757f10bd9102394d2a0b6bbc795f90444f66d2))
* **core:** Align concurrency and timeout defaults between instance and runner ([#&#8203;12503](https://github.com/n8n-io/n8n/issues/12503)) ([9953477](https://github.com/n8n-io/n8n/commit/9953477450c28ec2d211e55aadb825dbae2ee4d6))
* **core:** Allow `index` as top-level item key for Code node ([#&#8203;12469](https://github.com/n8n-io/n8n/issues/12469)) ([1b91000](https://github.com/n8n-io/n8n/commit/1b9100032fc9f8c33e263c8299e04054105da384))

[3.63.1]
* Update n8n to 1.74.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.74.3)
* **Postgres Chat Memory Node:** Do not terminate the connection pool ([#&#8203;12674](https://github.com/n8n-io/n8n/issues/12674)) ([61f8ea7](https://github.com/n8n-io/n8n/commit/61f8ea7e976c0db8d159aa12b23dae45d92fe916))
* **editor:** Defer crypto.randomUUID call in CodeNodeEditor ([#&#8203;12630](https://github.com/n8n-io/n8n/issues/12630)) ([0ffdcc5](https://github.com/n8n-io/n8n/commit/0ffdcc5ef466911a5f351f5c12ebf6da6645ee86))
* **editor:** Fix Code node bug erasing and overwriting code when switching between nodes ([#&#8203;12637](https://github.com/n8n-io/n8n/issues/12637)) ([ecb847b](https://github.com/n8n-io/n8n/commit/ecb847bc658d017f12689e8e9a016d367381220d))
* **Execute Workflow Node:** Pass binary data to sub-workflow ([#&#8203;12635](https://github.com/n8n-io/n8n/issues/12635)) ([2fd6418](https://github.com/n8n-io/n8n/commit/2fd6418ffc746a67747bcc116cc8bd8ec24ebb0a))
* **refactor(Postgres Node)**: Backport connection pooling to postgres v1 ([#&#8203;12484](https://github.com/n8n-io/n8n/pull/12484)) ([35cb10c](https://github.com/n8n-io/n8n/commit/35cb10c5e76d439ad484c549d542bdc97fe4a3ab))

[3.64.0]
* Update n8n to 1.75.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.75.2)
* **Postgres Chat Memory Node:** Do not terminate the connection pool ([#&#8203;12674](https://github.com/n8n-io/n8n/issues/12674)) ([47cf99e](https://github.com/n8n-io/n8n/commit/47cf99e20673dbf69836d310c0ffbf404dccee7b))
* **editor:** Defer crypto.randomUUID call in CodeNodeEditor ([#&#8203;12630](https://github.com/n8n-io/n8n/issues/12630)) ([4ba62e4](https://github.com/n8n-io/n8n/commit/4ba62e4d599abeea3e9ebee681141ecee44a30e1))
* **editor:** Fix Code node bug erasing and overwriting code when switching between nodes ([#&#8203;12637](https://github.com/n8n-io/n8n/issues/12637)) ([021e08f](https://github.com/n8n-io/n8n/commit/021e08f17c42c047ec63a061d7c99b8373823d48))
* **Execute Workflow Node:** Pass binary data to sub-workflow ([#&#8203;12635](https://github.com/n8n-io/n8n/issues/12635)) ([8007623](https://github.com/n8n-io/n8n/commit/80076237d607176d8069b58a0272a461443060e2))
* **refactor(Postgres Node)**: Backport connection pooling to postgres v1 ([#&#8203;12484](https://github.com/n8n-io/n8n/pull/12484)) ([35cb10c](https://github.com/n8n-io/n8n/commit/35cb10c5e76d439ad484c549d542bdc97fe4a3ab))
* **core:** AugmentObject should check for own propeties correctly ([#&#8203;12534](https://github.com/n8n-io/n8n/issues/12534)) ([0cdf393](https://github.com/n8n-io/n8n/commit/0cdf39374305e6bbcedb047db7d3756168e6e89e))
* **core:** Disallow code generation in task runner ([#&#8203;12522](https://github.com/n8n-io/n8n/issues/12522)) ([35b6180](https://github.com/n8n-io/n8n/commit/35b618098b7d23e272bf77b55c172dbe531c821f))
* **core:** Fix node exclusion on the frontend types ([#&#8203;12544](https://github.com/n8n-io/n8n/issues/12544)) ([b2cbed9](https://github.com/n8n-io/n8n/commit/b2cbed9865888f6f3bc528984d4091d86a88f0d6))
* **core:** Fix orchestration flow with expired license ([#&#8203;12444](https://github.com/n8n-io/n8n/issues/12444)) ([ecff3b7](https://github.com/n8n-io/n8n/commit/ecff3b732a028d7225bfbed4ffc65dc20c4ed608))
* **core:** Fix Sentry error reporting on task runners ([#&#8203;12495](https://github.com/n8n-io/n8n/issues/12495)) ([88c0838](https://github.com/n8n-io/n8n/commit/88c0838dd72f11646bdb3586223d6c16631cccab))

[3.65.0]
* Update n8n to 1.76.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.76.1)
* **Execute Workflow Node:** Pass binary data to sub-workflow ([#&#8203;12635](https://github.com/n8n-io/n8n/issues/12635)) ([e9c152e](https://github.com/n8n-io/n8n/commit/e9c152e369a4c2762bd8e6ad17eaa704bb3771bb))
* **Google Gemini Chat Model Node:** Add base URL support for Google Gemini Chat API ([#&#8203;12643](https://github.com/n8n-io/n8n/issues/12643)) ([14f4bc7](https://github.com/n8n-io/n8n/commit/14f4bc769027789513808b4000444edf99dc5d1c))
* **GraphQL Node:** Change default request format to json instead of graphql ([#&#8203;11346](https://github.com/n8n-io/n8n/issues/11346)) ([c7c122f](https://github.com/n8n-io/n8n/commit/c7c122f9173df824cc1b5ab864333bffd0d31f82))
* **Jira Software Node:** Get custom fields(RLC) in update operation for server deployment type ([#&#8203;12719](https://github.com/n8n-io/n8n/issues/12719)) ([353df79](https://github.com/n8n-io/n8n/commit/353df7941117e20547cd4f3fc514979a54619720))
* **n8n Form Node:** Remove the ability to change the formatting of dates ([#&#8203;12666](https://github.com/n8n-io/n8n/issues/12666)) ([14904ff](https://github.com/n8n-io/n8n/commit/14904ff77951fef23eb789a43947492a4cd3fa20))
* **OpenAI Chat Model Node:** Fix loading of custom models when using custom credential URL ([#&#8203;12634](https://github.com/n8n-io/n8n/issues/12634)) ([7cc553e](https://github.com/n8n-io/n8n/commit/7cc553e3b277a16682bfca1ea08cb98178e38580))
* **OpenAI Chat Model Node:** Restore default model value ([#&#8203;12745](https://github.com/n8n-io/n8n/issues/12745)) ([d1b6692](https://github.com/n8n-io/n8n/commit/d1b6692736182fa2eab768ba3ad0adb8504ebbbd))
* **Postgres Chat Memory Node:** Do not terminate the connection pool ([#&#8203;12674](https://github.com/n8n-io/n8n/issues/12674)) ([e7f00bc](https://github.com/n8n-io/n8n/commit/e7f00bcb7f2dce66ca07a9322d50f96356c1a43d))
* **Postgres Node:** Allow using composite key in upsert queries ([#&#8203;12639](https://github.com/n8n-io/n8n/issues/12639)) ([83ce3a9](https://github.com/n8n-io/n8n/commit/83ce3a90963ba76601234f4314363a8ccc310f0f))
* **Wait Node:** Fix for hasNextPage in waiting forms ([#&#8203;12636](https://github.com/n8n-io/n8n/issues/12636)) ([652b8d1](https://github.com/n8n-io/n8n/commit/652b8d170b9624d47b5f2d8d679c165cc14ea548))
* Add credential only node for Microsoft Azure Monitor ([#&#8203;12645](https://github.com/n8n-io/n8n/issues/12645)) ([6ef8882](https://github.com/n8n-io/n8n/commit/6ef8882a108c672ab097c9dd1c590d4e9e7f3bcc))
* Add Miro credential only node ([#&#8203;12746](https://github.com/n8n-io/n8n/issues/12746)) ([5b29086](https://github.com/n8n-io/n8n/commit/5b29086e2f9b7f638fac4440711f673438e57492))
* Add SSM endpoint to AWS credentials ([#&#8203;12212](https://github.com/n8n-io/n8n/issues/12212)) ([565c7b8](https://github.com/n8n-io/n8n/commit/565c7b8b9cfd3e10f6a2c60add96fea4c4d95d33))
* **core:** Enable task runner by default ([#&#8203;12726](https://github.com/n8n-io/n8n/issues/12726)) ([9e2a01a](https://github.com/n8n-io/n8n/commit/9e2a01aeaf36766a1cf7a1d9a4d6e02f45739bd3))
* **editor:** Force final canvas v2 migration and remove switcher from UI ([#&#8203;12717](https://github.com/n8n-io/n8n/issues/12717)) ([29335b9](https://github.com/n8n-io/n8n/commit/29335b9b6acf97c817bea70688e8a2786fbd8889))
* **editor:** VariablesView Reskin - Add Filters for missing values ([#&#8203;12611](https://github.com/n8n-io/n8n/issues/12611)) ([1eeb788](https://github.com/n8n-io/n8n/commit/1eeb788d327287d21eab7ad6f2156453ab7642c7))
* **Jira Software Node:** Personal Access Token credential type ([#&#8203;11038](https://github.com/n8n-io/n8n/issues/11038)) ([1c7a38f](https://github.com/n8n-io/n8n/commit/1c7a38f6bab108daa47401cd98c185590bf299a8))
* **n8n Form Trigger Node:** Form Improvements ([#&#8203;12590](https://github.com/n8n-io/n8n/issues/12590)) ([f167578](https://github.com/n8n-io/n8n/commit/f167578b3251e553a4d000e731e1bb60348916ad))
* Synchronize deletions when pulling from source control ([#&#8203;12170](https://github.com/n8n-io/n8n/issues/12170)) ([967ee4b](https://github.com/n8n-io/n8n/commit/967ee4b89b94b92fc3955c56bf4c9cca0bd64eac))

[3.66.0]
* N8N_CONFIG_FILES is deprecated

[3.66.1]
* Update n8n to 1.76.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.76.2)
* **core:** Fix empty node execution stack ([#&#8203;12945](https://github.com/n8n-io/n8n/issues/12945)) ([504746d](https://github.com/n8n-io/n8n/commit/504746de2d7f0de0a2e3c64a929b8ec727dbf852))
* **editor:** Fix execution running status listener for chat messages ([#&#8203;12951](https://github.com/n8n-io/n8n/issues/12951)) ([4157d79](https://github.com/n8n-io/n8n/commit/4157d793b14e39ad37a8fdb6289491c43305b2a8))
* **editor:** Fix showing and hiding canvas edge toolbar when hovering ([#&#8203;13009](https://github.com/n8n-io/n8n/issues/13009)) ([7a08b45](https://github.com/n8n-io/n8n/commit/7a08b45406e6b1f1afe9f20f15a12cd00d4fa824))

[3.66.2]
* Update n8n to 1.76.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.76.3)
* **core:** Do not enable strict type validation by default for resource mapper ([#&#8203;13037](https://github.com/n8n-io/n8n/issues/13037)) ([25edde9](https://github.com/n8n-io/n8n/commit/25edde9ecfc470f5a145733665ca3bdf68248c3c))
* Increment runIndex in WorkflowToolV2 tool executions to avoid reusing out of date inputs ([#&#8203;13008](https://github.com/n8n-io/n8n/issues/13008)) ([a4e45ac](https://github.com/n8n-io/n8n/commit/a4e45ace908a9dd338c5facdc3305a4efc064f1d))

[3.67.0]
* Update n8n to 1.77.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.77.3)
* **core:** Only use new resource mapper type validation when it is enabled ([#&#8203;13099](https://github.com/n8n-io/n8n/issues/13099)) ([cf0e3ff](https://github.com/n8n-io/n8n/commit/cf0e3fffbb4f7b6a9dde96a8b0bf9168fee2e7cf))
* **core:** Do not enable strict type validation by default for resource mapper ([#&#8203;13037](https://github.com/n8n-io/n8n/issues/13037)) ([582dc44](https://github.com/n8n-io/n8n/commit/582dc443668a30e645abbadc0c50d96dbb913bce))
* Increment runIndex in WorkflowToolV2 tool executions to avoid reusing out of date inputs ([#&#8203;13008](https://github.com/n8n-io/n8n/issues/13008)) ([82f17fe](https://github.com/n8n-io/n8n/commit/82f17fe5931c5ca09c8ccefb80c873d1e78a22b8))
* **core:** Fix empty node execution stack ([#&#8203;12945](https://github.com/n8n-io/n8n/issues/12945)) ([d96e7c4](https://github.com/n8n-io/n8n/commit/d96e7c48a34a139ba16fcd72800de09ca6dd0511))
* **editor:** Fix execution running status listener for chat messages ([#&#8203;12951](https://github.com/n8n-io/n8n/issues/12951)) ([5261588](https://github.com/n8n-io/n8n/commit/5261588ac3ef2bdd8c6dd72cad1ad5130f87c384))
* **editor:** Fix showing and hiding canvas edge toolbar when hovering ([#&#8203;13009](https://github.com/n8n-io/n8n/issues/13009)) ([457f31c](https://github.com/n8n-io/n8n/commit/457f31c7db5907e874c5ebb478e87e43580302a5))
* **core:** Account for pre-execution failure in scaling mode ([#&#8203;12815](https://github.com/n8n-io/n8n/issues/12815)) ([b4d27c4](https://github.com/n8n-io/n8n/commit/b4d27c49e32bfacbd2690bf1c07194562f6a4a61))
* **core:** Display the last activated plan name when multiple are activated ([#&#8203;12835](https://github.com/n8n-io/n8n/issues/12835)) ([03365f0](https://github.com/n8n-io/n8n/commit/03365f096d3d5c8e3a6537f37cda412959705346))
* **core:** Fix possible corruption of OAuth2 credential ([#&#8203;12880](https://github.com/n8n-io/n8n/issues/12880)) ([ac84ea1](https://github.com/n8n-io/n8n/commit/ac84ea14452cbcec95f14073e8e70427169e6a7f))
* **core:** Fix usage of external libs in task runner ([#&#8203;12788](https://github.com/n8n-io/n8n/issues/12788)) ([3d9d5bf](https://github.com/n8n-io/n8n/commit/3d9d5bf9d58f3c49830d42a140d6c8c6b59952dc))
* **core:** Handle max stalled count error better ([#&#8203;12824](https://github.com/n8n-io/n8n/issues/12824)) ([eabf160](https://github.com/n8n-io/n8n/commit/eabf1609577cd94a6bad5020c34378d840a13bc0))
* **core:** Improve error handling in credential decryption and parsing ([#&#8203;12868](https://github.com/n8n-io/n8n/issues/12868)) ([0c86bf2](https://github.com/n8n-io/n8n/commit/0c86bf2b3761bb93fd3cedba7a483ae5d97bd332))

[3.68.0]
* Update n8n to 1.78.0
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.78.0)
* **AI Agent Node:** Ignore SSL errors option for SQLAgent ([#&#8203;13052](https://github.com/n8n-io/n8n/issues/13052)) ([a90529f](https://github.com/n8n-io/n8n/commit/a90529fd51ca88bc9640d24490dbeb2023c98e30))
* **Code Node:** Do not validate code within comments ([#&#8203;12938](https://github.com/n8n-io/n8n/issues/12938)) ([cdfa225](https://github.com/n8n-io/n8n/commit/cdfa22593b69cf647c2a798d6571a9bbbd11c1b2))
* **core:** "Respond to Webhook" should work with workflows with waiting nodes ([#&#8203;12806](https://github.com/n8n-io/n8n/issues/12806)) ([e8635f2](https://github.com/n8n-io/n8n/commit/e8635f257433748f4d7d2c4b0ae794de6bff5b28))
* **core:** Do not emit `workflow-post-execute` event for waiting executions ([#&#8203;13065](https://github.com/n8n-io/n8n/issues/13065)) ([1593b6c](https://github.com/n8n-io/n8n/commit/1593b6cb4112ab2a85ca93c4eaec7d5f088895b1))
* **core:** Do not enable strict type validation by default for resource mapper ([#&#8203;13037](https://github.com/n8n-io/n8n/issues/13037)) ([fdcff90](https://github.com/n8n-io/n8n/commit/fdcff9082b97314f8b04579ab6fa81c724916320))
* **core:** Fix empty node execution stack ([#&#8203;12945](https://github.com/n8n-io/n8n/issues/12945)) ([7031569](https://github.com/n8n-io/n8n/commit/7031569a028bcc85558fcb614f8143d68a7f81f0))
* **core:** Only use new resource mapper type validation when it is enabled ([#&#8203;13099](https://github.com/n8n-io/n8n/issues/13099)) ([a37c8e8](https://github.com/n8n-io/n8n/commit/a37c8e8fb86aaa3244ac13500ffa0e7c0d809a6f))
* **editor:** Actually enforce the version and don't break for old values in local storage ([#&#8203;13025](https://github.com/n8n-io/n8n/issues/13025)) ([884a7e2](https://github.com/n8n-io/n8n/commit/884a7e23f84258756d8dcdd2dfe933bdedf61adc))
* **editor:** Add telemetry to source control feature ([#&#8203;13016](https://github.com/n8n-io/n8n/issues/13016)) ([18eaa54](https://github.com/n8n-io/n8n/commit/18eaa5423dfc9348374c2cff4ae0e6f152268fbb))
* **editor:** Allow switch to `Fixed` for boolean and number parameters with invalid expressions ([#&#8203;12948](https://github.com/n8n-io/n8n/issues/12948)) ([118be24](https://github.com/n8n-io/n8n/commit/118be24d25f001525ced03d9426a6129fa5a2053))
* **editor:** Allow to re-open sub-connection node creator if already active ([#&#8203;13041](https://github.com/n8n-io/n8n/issues/13041)) ([16d59e9](https://github.com/n8n-io/n8n/commit/16d59e98edc427bf68edbce4cd2174a44d6dcfb1))
* **editor:** Code node overwrites code when switching nodes after edits ([#&#8203;13078](https://github.com/n8n-io/n8n/issues/13078)) ([00e3ebc](https://github.com/n8n-io/n8n/commit/00e3ebc9e2e0b8cc2d88b678c3a2a21602dac010))
* **editor:** Fix execution running status listener for chat messages ([#&#8203;12951](https://github.com/n8n-io/n8n/issues/12951)) ([4d55a29](https://github.com/n8n-io/n8n/commit/4d55a294600dc2c86f6f7019da923b66a4b9de7e))
* **editor:** Fix position of connector buttons when the line is straight ([#&#8203;13034](https://github.com/n8n-io/n8n/issues/13034)) ([3a908ac](https://github.com/n8n-io/n8n/commit/3a908aca17f0bc1cf5fb5eb8813cc94f27f0bcdf))
* **editor:** Fix showing and hiding canvas edge toolbar when hovering ([#&#8203;13009](https://github.com/n8n-io/n8n/issues/13009)) ([ac7bc4f](https://github.com/n8n-io/n8n/commit/ac7bc4f1911f913233eeeae5d229432fdff332c4))
* **editor:** Make AI transform node read only in executions view ([#&#8203;12970](https://github.com/n8n-io/n8n/issues/12970)) ([ce1deb8](https://github.com/n8n-io/n8n/commit/ce1deb8aea528eef996fc774d0fff1dc61df5843))
* **editor:** Prevent infinite loop in expressions crashing the browser ([#&#8203;12732](https://github.com/n8n-io/n8n/issues/12732)) ([8c2dbcf](https://github.com/n8n-io/n8n/commit/8c2dbcfeced70a0a84137773269cc6db2928d174))
* **editor:** Refine push modal layout ([#&#8203;12886](https://github.com/n8n-io/n8n/issues/12886)) ([212a5bf](https://github.com/n8n-io/n8n/commit/212a5bf23eb11cc3296e7a8d002a4b7727d5193c))
* **editor:** SchemaView renders duplicate structures properly ([#&#8203;12943](https://github.com/n8n-io/n8n/issues/12943)) ([0d8a544](https://github.com/n8n-io/n8n/commit/0d8a544975f72724db931778d7e3ace8a12b6cfc))
* **editor:** Update node issues when opening execution ([#&#8203;12972](https://github.com/n8n-io/n8n/issues/12972)) ([1a91523](https://github.com/n8n-io/n8n/commit/1a915239c6571d7744023c6df6242dabe97c912e))
* **editor:** Use correct connection index when connecting adjancent nodes after deleting a node ([#&#8203;12973](https://github.com/n8n-io/n8n/issues/12973)) ([c7a15d5](https://github.com/n8n-io/n8n/commit/c7a15d5980d181a865f8e2ec6a5f70d0681dcf56))
* **GitHub Node:** Don't truncate filenames retrieved from GitHub ([#&#8203;12923](https://github.com/n8n-io/n8n/issues/12923)) ([7e18447](https://github.com/n8n-io/n8n/commit/7e1844757fe0d544e8881d229d16af95ed53fb21))
* **Google Cloud Firestore Node:** Fix potential prototype pollution vulnerability ([#&#8203;13035](https://github.com/n8n-io/n8n/issues/13035)) ([f150f79](https://github.com/n8n-io/n8n/commit/f150f79ad6c7d43e036688b1de8d6c2c8140aca9))
* Increment runIndex in WorkflowToolV2 tool executions to avoid reusing out of date inputs ([#&#8203;13008](https://github.com/n8n-io/n8n/issues/13008)) ([cc907fb](https://github.com/n8n-io/n8n/commit/cc907fbca9aa00fe07dd54a2fcac8983f2321ad1))
* Sync partial execution version of FE and BE, also allow enforcing a specific version ([#&#8203;12840](https://github.com/n8n-io/n8n/issues/12840)) ([a155043](https://github.com/n8n-io/n8n/commit/a15504329bac582225185705566297d9cc27bf73))
* **Wise Node:** Use ISO formatting for timestamps ([#&#8203;10288](https://github.com/n8n-io/n8n/issues/10288)) ([1a2d39a](https://github.com/n8n-io/n8n/commit/1a2d39a158c9a61bdaf11124b09ae70de65ebbf1))
* Add reusable frontend `composables` package ([#&#8203;13077](https://github.com/n8n-io/n8n/issues/13077)) ([ef87da4](https://github.com/n8n-io/n8n/commit/ef87da4c193a08e089e48044906a4f5ce9959a22))
* Add support for client credentials with Azure Log monitor ([#&#8203;13038](https://github.com/n8n-io/n8n/issues/13038)) ([2c2d631](https://github.com/n8n-io/n8n/commit/2c2d63157b7866f1a68cc45c5823e29570ccff77))
* Allow multi API creation via the UI ([#&#8203;12845](https://github.com/n8n-io/n8n/issues/12845)) ([ad3250c](https://github.com/n8n-io/n8n/commit/ad3250ceb0df84379917e684d54d4100e3bf44f5))
* Allow setting API keys expiration ([#&#8203;12954](https://github.com/n8n-io/n8n/issues/12954)) ([9bcbc2c](https://github.com/n8n-io/n8n/commit/9bcbc2c2ccbb88537e9b7554c92b631118d870f1))
* **core:** Add sorting to GET `/workflows` endpoint ([#&#8203;13029](https://github.com/n8n-io/n8n/issues/13029)) ([b60011a](https://github.com/n8n-io/n8n/commit/b60011a1808d47f32ab84e685dba0e915e82df8f))
* **core:** Enable usage as a tool for more nodes ([#&#8203;12930](https://github.com/n8n-io/n8n/issues/12930)) ([9deb759](https://github.com/n8n-io/n8n/commit/9deb75916e4eb63b899ba79b40cbd24b69a752db))
* **core:** Handle Declarative nodes more like regular nodes ([#&#8203;13007](https://github.com/n8n-io/n8n/issues/13007)) ([a65a9e6](https://github.com/n8n-io/n8n/commit/a65a9e631b13bbe70ad64727fb1109ae7cd014eb))
* **Discord Node:** New sendAndWait operation ([#&#8203;12894](https://github.com/n8n-io/n8n/issues/12894)) ([d47bfdd](https://github.com/n8n-io/n8n/commit/d47bfddd656367454b51da39cf87dbfb2bd59eb2))
* **editor:** Display schema preview for unexecuted nodes ([#&#8203;12901](https://github.com/n8n-io/n8n/issues/12901)) ([0063bbb](https://github.com/n8n-io/n8n/commit/0063bbb30b45b3af92aff4c0f76b905d50a71a2d))
* **editor:** Easy $fromAI Button for AI Tools ([#&#8203;12587](https://github.com/n8n-io/n8n/issues/12587)) ([2177376](https://github.com/n8n-io/n8n/commit/21773764d37c37a6464a3885d3fa548a5feb4fd8))
* **editor:** Show fixed collection parameter issues in UI ([#&#8203;12899](https://github.com/n8n-io/n8n/issues/12899)) ([12d686c](https://github.com/n8n-io/n8n/commit/12d686ce52694f4c0b88f92a744451c1b0c66dec))
* **Facebook Graph API Node:** Update node to support API v22.0 ([#&#8203;13024](https://github.com/n8n-io/n8n/issues/13024)) ([0bc0fc6](https://github.com/n8n-io/n8n/commit/0bc0fc6c1226688c29bf5f8f0ba7e8f244e16fbc))
* **HTTP Request Tool Node:** Relax binary data detection ([#&#8203;13048](https://github.com/n8n-io/n8n/issues/13048)) ([b67a003](https://github.com/n8n-io/n8n/commit/b67a003e0b154d4e8c04392bec1c7b28171b5908))
* Human in the loop section ([#&#8203;12883](https://github.com/n8n-io/n8n/issues/12883)) ([9590e5d](https://github.com/n8n-io/n8n/commit/9590e5d58b8964de9ce901bf07b537926d18b6b7))
* **n8n Form Node:** Add Hidden Fields ([#&#8203;12803](https://github.com/n8n-io/n8n/issues/12803)) ([0da1114](https://github.com/n8n-io/n8n/commit/0da1114981978e371b216bdabc0c3bbdceeefa09))
* **n8n Form Node:** Respond with Text ([#&#8203;12979](https://github.com/n8n-io/n8n/issues/12979)) ([182fc15](https://github.com/n8n-io/n8n/commit/182fc150bec62e9a5e2801d6c403e4a6bd35f728))
* **OpenAI Chat Model Node, OpenAI Node:** Include o3 models in model selection ([#&#8203;13005](https://github.com/n8n-io/n8n/issues/13005)) ([37d152c](https://github.com/n8n-io/n8n/commit/37d152c148cafbe493c22e07f5d55ff24fcb0ca4))
* **Summarize Node:** Preserves original field data type ([#&#8203;13069](https://github.com/n8n-io/n8n/issues/13069)) ([be5e49d](https://github.com/n8n-io/n8n/commit/be5e49d56c09d65c9768e948471626cfd3606c0c))

[3.68.1]
* Update n8n to 1.78.1
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.78.1)
* **core:** Redact credentials ([#&#8203;13263](https://github.com/n8n-io/n8n/issues/13263)) ([1756097](https://github.com/n8n-io/n8n/commit/17560979cdb12e71a74c9fb41abceed3c6bee0c2))

[3.69.0]
* Update n8n to 1.79.2
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.79.2)
* Always clear popupWindowState  before showing popup from form trigger ([#&#8203;13363](https://github.com/n8n-io/n8n/issues/13363)) ([f65b0f5](https://github.com/n8n-io/n8n/commit/f65b0f5e0b10d4d6c2bea6d0026f7b624eb9bdfe))
* **Code Node:** Fix `$items` in Code node when using task runner ([#&#8203;13368](https://github.com/n8n-io/n8n/issues/13368)) ([172fbe5](https://github.com/n8n-io/n8n/commit/172fbe5f7bddfd3cc8a725d81b22e65c85b1e082))
* **core:** Ensure that 'workflow-post-execute' event has userId whenever it's available ([#&#8203;13326](https://github.com/n8n-io/n8n/issues/13326)) ([c2ed1a0](https://github.com/n8n-io/n8n/commit/c2ed1a04cbfd5221c9b8935d4f0545c7ea67e811))
* **core:** Handle connections for missing nodes in a workflow ([#&#8203;13362](https://github.com/n8n-io/n8n/issues/13362)) ([4229d6c](https://github.com/n8n-io/n8n/commit/4229d6c804db0072f5eab5e33b410f99af542829))

[3.69.1]
* Update n8n to 1.79.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.79.3)
* **core:** Make sure middleware works with legacy API Keys ([#&#8203;13390](https://github.com/n8n-io/n8n/issues/13390)) ([537df8a](https://github.com/n8n-io/n8n/commit/537df8a745ec4956da9898593c962d16e2d4097b))

[3.69.2]
* Update n8n to 1.79.4
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.79.4)

[3.70.0]
* Update n8n to 1.80.3
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.80.3)
* **core:** Make sure middleware works with legacy API Keys ([#&#8203;13390](https://github.com/n8n-io/n8n/issues/13390)) ([c4fe0b5](https://github.com/n8n-io/n8n/commit/c4fe0b550cda96b288ce17d5d46d2fa7b6e72eb5))
* Always clear popupWindowState  before showing popup from form trigger ([#&#8203;13363](https://github.com/n8n-io/n8n/issues/13363)) ([44e583b](https://github.com/n8n-io/n8n/commit/44e583bbb79c328ee9b8a69afb7b059106cb1613))
* **Code Node:** Fix `$items` in Code node when using task runner ([#&#8203;13368](https://github.com/n8n-io/n8n/issues/13368)) ([da5afd0](https://github.com/n8n-io/n8n/commit/da5afd06a943d29c6a57e590aa8a2d91264329dc))
* **core:** Ensure that 'workflow-post-execute' event has userId whenever it's available ([#&#8203;13326](https://github.com/n8n-io/n8n/issues/13326)) ([088872b](https://github.com/n8n-io/n8n/commit/088872b086d5d794e7a19af5e52ae75d2b220f17))
* **core:** Handle connections for missing nodes in a workflow ([#&#8203;13362](https://github.com/n8n-io/n8n/issues/13362)) ([4da7ed7](https://github.com/n8n-io/n8n/commit/4da7ed7ebc4e206d4a1a35c968b9dbbded72eb81))
* **AI Agent Node:** Move model retrieval into try/catch to fix continueOnFail handling ([#&#8203;13165](https://github.com/n8n-io/n8n/issues/13165)) ([47c5688](https://github.com/n8n-io/n8n/commit/47c5688618001a51c9412c5d07fd25d85b8d1b8d))
* **Code Tool Node:** Fix Input Schema Parameter not hiding correctly ([#&#8203;13245](https://github.com/n8n-io/n8n/issues/13245)) ([8e15ebf](https://github.com/n8n-io/n8n/commit/8e15ebf8333d06b5fe4d5bf8ee39f285b31332d7))

[3.70.1]
* Update n8n to 1.80.4
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.80.4)
* **Call n8n Workflow Tool Node:** Support concurrent invocations of the tool ([#&#8203;13526](https://github.com/n8n-io/n8n/issues/13526)) ([de4d26d](https://github.com/n8n-io/n8n/commit/de4d26dc5f42ee572a509bdd5932e9564f5a4aee))

[3.70.2]
* Update n8n to 1.80.5
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.80.5)
* **Postgres Node:** Accommodate null values in query parameters for expressions ([#&#8203;13544](https://github.com/n8n-io/n8n/issues/13544)) ([1940b09](https://github.com/n8n-io/n8n/commit/1940b098e89a6d38ce932d8e5ed7c2d597113623))

[3.71.0]
* Update n8n to 1.81.4
* [Full Changelog](https://github.com/n8n-io/n8n/releases/tag/n8n@1.81.4)
* **core:** Do not validate email when LDAP is enabled ([#&#8203;13605](https://github.com/n8n-io/n8n/issues/13605)) ([c55330c](https://github.com/n8n-io/n8n/commit/c55330c95770a7191fdcfd4d09a612c1f1690273))
* **core:** Gracefully handle missing tasks metadata ([#&#8203;13632](https://github.com/n8n-io/n8n/issues/13632)) ([cecf61a](https://github.com/n8n-io/n8n/commit/cecf61a6d7d5ac759f5af278549566013e930bbd))
* **n8n Form Trigger Node:** Sanitize HTML for formNode ([#&#8203;13595](https://github.com/n8n-io/n8n/issues/13595)) ([d0074d6](https://github.com/n8n-io/n8n/commit/d0074d6d86c10dce9dd53f8198cf4aeeab618ae8))
* **editor:** Add workflows to the store when fetching current page ([#&#8203;13583](https://github.com/n8n-io/n8n/issues/13583)) ([127f213](https://github.com/n8n-io/n8n/commit/127f21366c1c66471f379a59b07587d8380e2ed9))
* **editor:** Undo keybinding changes related to window focus/blur events ([#&#8203;13559](https://github.com/n8n-io/n8n/issues/13559)) ([ee07621](https://github.com/n8n-io/n8n/commit/ee07621df1921dabe93a57dc367a051669ef650b))
* **Postgres Node:** Accommodate null values in query parameters for expressions ([#&#8203;13544](https://github.com/n8n-io/n8n/issues/13544)) ([bdb1870](https://github.com/n8n-io/n8n/commit/bdb1870e7feb187863fabb6ee8a20fcb381b5c2d))

[3.72.0]
* Update base image to 5.0.0

