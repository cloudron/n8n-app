#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    fs = require('fs'),
    expect = require('expect.js'),
    path = require('path'),
    timers = require('timers/promises'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'admin@cloudron.local';
    const PASSWORD = 'Test1234';
    const FIRST_NAME = 'Herbert';
    const LAST_NAME = 'Cloudroner';
    const workflow_file_url = 'https://git.cloudron.io/cloudron/n8n-app/-/raw/master/test/Cloudron_Test_Workflow.json';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function waitForElement(elem) {
        await timers.setTimeout(2000);
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function setup() {
        await browser.get(`https://${app.fqdn}/setup`);

        await waitForElement(By.xpath('//input[@autocomplete="email"]'));

        await browser.findElement(By.xpath('//input[@autocomplete="email"]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//input[@autocomplete="given-name"]')).sendKeys(FIRST_NAME);
        await browser.findElement(By.xpath('//input[@autocomplete="family-name"]')).sendKeys(LAST_NAME);
        await browser.findElement(By.xpath('//input[@autocomplete="new-password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//span[text()="Next"]')).click();

        // initials from FIRST_NAME and LAST_NAME
        await waitForElement(By.xpath('//span[text()="HC"]'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/signin`);

        await waitForElement(By.xpath('//input[@autocomplete="email"]'));

        await browser.findElement(By.xpath('//input[@autocomplete="email"]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//input[@autocomplete="current-password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button/span[text()="Sign in"]')).click();

        await waitForElement(By.xpath('//span[text()="HC"]'));
    }

    async function openWorkflow() {
        await browser.get(`https://${app.fqdn}/workflows`);

        // Find element with text "Cloudron Test Workflow" and click it.
        await waitForElement(By.xpath('//h2[contains(.,"My workflow")]'));
        await browser.findElement(By.xpath('//h2[contains(.,"My workflow")]')).click();

        await waitForElement(By.xpath('//span[@title="My workflow"]'));
        await timers.setTimeout(500);
    }

    async function importWorkflowFromUrl() {
        await browser.get(`https://${app.fqdn}/workflow/new`);

        await waitForElement(By.xpath('//span[@class="actions"]//div[@class="el-dropdown"]'));
        await browser.findElement(By.xpath('//span[@class="actions"]//div[@class="el-dropdown"]')).click();

        // click Import from URL
        await waitForElement(By.xpath('//li[@role="menuitem"]//span[contains(text(),"Import from URL...")]'));
        await browser.findElement(By.xpath('//li[@role="menuitem"]//span[contains(text(),"Import from URL...")]')).click();

        // Paste URL for file
        await waitForElement(By.xpath('//div[@class="el-message-box__input"]//input'));
        await browser.findElement(By.xpath('//div[@class="el-message-box__input"]//input')).sendKeys(workflow_file_url);

        // click import
        await waitForElement(By.xpath('//button/span[text()="Import"]'));
        await browser.findElement(By.xpath('//button/span[text()="Import"]')).click();

        // Activate Workflow
        await waitForElement(By.xpath('//div[@title="Activate workflow"] | //div[@title="Activate Workflow"]'));
        await browser.findElement(By.xpath('//div[@title="Activate workflow"] | //div[@title="Activate Workflow"]')).click();

        // wait for saving
        await timers.setTimeout(1000);
    }

    async function checkWorkflowData(execNumber='1') {
        console.log(`Sleeping for 30sec to let the imported workflow generate some data in execution ${execNumber} . ${(new Date()).toString()}`);
        await timers.setTimeout(30000);

        await browser.get(`https://${app.fqdn}/home/executions`);
        await waitForElement(By.xpath('//span[text()="Executions"]'));

        await waitForElement(By.xpath('//td/span[.="My workflow"]'));
        await waitForElement(By.xpath('//span[contains(text(), "Succeeded")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can get app information', getAppInfo);
    it('can setup', setup);
    // it('can login', login);
    it('can import workflow from URL', importWorkflowFromUrl);
    it('check if workflow created data', checkWorkflowData);
    it('can logout', clearCache);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can login', login);
    it('can open imported workflow', openWorkflow);
    it('check if workflow creates data', checkWorkflowData.bind(null, '3'));
    it('can logout', clearCache);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can login', login);
    it('can open imported workflow', openWorkflow);
    it('check if workflow creates data', checkWorkflowData.bind(null, '5'));
    it('can logout', clearCache);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can open imported workflow', openWorkflow);
    it('check if workflow creates data', checkWorkflowData.bind(null, '7'));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id io.n8n.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);
    it('can setup', setup);
    it('can import workflow from URL', importWorkflowFromUrl);
    it('check if workflow created data', checkWorkflowData);
    it('can logout', clearCache);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can login', login);
    it('can open imported workflow', openWorkflow);
    it('check if workflow creates data', checkWorkflowData.bind(null, '3'));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
