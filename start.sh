#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /run/root.npm /run/cloudron.npm /app/data/user/.n8n /app/data/custom-extensions /app/data/configs /run/cloudron.cache /run/n8n/public /run/cloudron.ssh

echo "=> Loading configuration"
export VUE_APP_URL_BASE_API="${CLOUDRON_APP_ORIGIN}/"
export WEBHOOK_URL="${CLOUDRON_APP_ORIGIN}/"
export N8N_VERSION_NOTIFICATIONS_ENABLED=false
export N8N_DIAGNOSTICS_ENABLED=false
# already set in Dockerfile
# export N8N_CUSTOM_EXTENSIONS="/app/data/custom-extensions"
# export N8N_USER_FOLDER="/app/data/user" # always uses .n8n underneath
export N8N_LOG_OUTPUT="console"
export N8N_EMAIL_MODE="smtp"
export N8N_SMTP_HOST="${CLOUDRON_MAIL_SMTP_SERVER}"
export N8N_SMTP_PORT="${CLOUDRON_MAIL_SMTP_PORT}"
export N8N_SMTP_USER="${CLOUDRON_MAIL_SMTP_USERNAME}"
export N8N_SMTP_PASS="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export N8N_SMTP_SENDER="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-n8n} <${CLOUDRON_MAIL_FROM}>"
export N8N_SMTP_SSL=false

if [[ ! -f "/app/data/env.sh" ]]; then
    echo "=> Creating env.sh template on first run"
    cp /app/pkg/env.sh.template /app/data/env.sh
fi

source /app/data/env.sh

export DB_TYPE=postgresdb
export DB_POSTGRESDB_DATABASE="${CLOUDRON_POSTGRESQL_DATABASE}"
export DB_POSTGRESDB_HOST="${CLOUDRON_POSTGRESQL_HOST}"
export DB_POSTGRESDB_PORT="${CLOUDRON_POSTGRESQL_PORT}"
export DB_POSTGRESDB_USER="${CLOUDRON_POSTGRESQL_USERNAME}"
export DB_POSTGRESDB_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"

echo "=> Setting permissions"
chown -R cloudron:cloudron /app/data /run/n8n /run/*.npm /run/*.cache /run/*.ssh

if [[ -n "${EXTRA_NODE_MODULES:-}" ]]; then
    echo "=> Installing ${EXTRA_NODE_MODULES}"
    gosu cloudron:cloudron npm install ${EXTRA_NODE_MODULES} # do not quote it
fi

echo "=> Starting N8N"
exec gosu cloudron:cloudron /app/code/node_modules/.bin/n8n start

